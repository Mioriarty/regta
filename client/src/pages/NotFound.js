import { Button, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NotFound() {
    return (
        <Row className="justify-content-center">
            <div style={{ width: '40rem', marginTop: '40px' }} className="text-center">
                <hr />
                <h1>Die Seite, nach der Du suchst, existiert nicht!</h1> 
                <hr />
                <br /><br />
                <Link to='/'><Button variant="outline-secondary">Zurück</Button></Link>
            </div>
        </Row>
    );
}