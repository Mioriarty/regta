import { useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import api from "../../service/api";
import OtherEdit from "../../components/forms/dataEdits/OtherEdit";
import { Link } from "react-router-dom";
import usePushNotificator from "../../hooks/usePushNotificator";

export default function NewOther() {
    const [ name, setName ] = useState('');
    const [ pwd, setPwd ]   = useState('');
    const [ pwd2, setPwd2 ] = useState('');

    const [ pushNotification, Notificator ] = usePushNotificator();

    const handleSubmit = async e => {
        e.preventDefault();
        if(pwd !== pwd2) {
            pushNotification('Ungültige Eingabe', 'Die Passwörter müssen übereinstimmen!', 'warning');
        } else {
            const res = await api.post('others', {name: name, pwd: pwd});

            if(res.status === 'ok') {
                setName('');
                setPwd('');
                setPwd2('');

                pushNotification('Bearbeitung erfolgreich', 'Benutzer erfolgreich erstellt', 'success');
            } else {
                pushNotification('Interner Fehler', res.error, 'danger');
            }
            
        }
        
    }

    return (
        <>
        <Notificator />
        <Card body>
            <Card.Title>Neuen Benutzer erstellen</Card.Title>
            <hr />
            <Form onSubmit={handleSubmit}>
                <OtherEdit name={name} setName={setName} pwd={pwd} setPwd={setPwd} pwd2={pwd2} setPwd2={setPwd2} mode='new' />
                <div className="text-end">
                    <Link to="/"><Button variant="secondary" className="me-2">Zurück</Button></Link>
                    <Button type="submit" variant="success">Erstellen</Button>
                </div>
            </Form>
        </Card>
        </>
    );
}