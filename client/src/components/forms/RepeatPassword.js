import { Form, InputGroup } from "react-bootstrap";

export default function RepeatPassword(props) {
    return (
        <InputGroup >
            <Form.Control type="password" placeholder="Passwort widerholen" value={props.pwd2} onChange={e => props.setPwd2(e.target.value)} required/>
            <InputGroup.Text>{props.pwd !== '' && props.pwd === props.pwd2 ? '✔' : '⚠'}</InputGroup.Text>
        </InputGroup>
    );
}