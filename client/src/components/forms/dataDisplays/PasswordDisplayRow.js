import FormRow from "../../bt/FormRow";

export default function PasswordDisplayRow() {
    return (
        <FormRow label="Passwort" plaintext>
            ●●●●●●●●
        </FormRow>
    )
}