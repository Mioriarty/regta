import { Form } from "react-bootstrap";
import useFetchData from "../../../hooks/useFetchData"
import FloatingSelect from '../../bt/FloatingSelect'

export default function OtherSelect({ defaultFirst, floating, value, setValue, dependencies, ...rest }) {
    
    const others = useFetchData('others', {}, dependencies, first => {if(defaultFirst) setValue(first.id)})
    const SelectInUse = floating ? FloatingSelect : Form.Select;

    return (
        <SelectInUse value={value} onChange={e => setValue(e.target.value)} {...rest}>
            {defaultFirst ? <></> : <option disabled="disabled" value=''>Benutzer auswählen</option>}
            {others.map(o => <option value={o.id} key={o.id}>{o.name}</option>)}
        </SelectInUse>
    )
}

OtherSelect.defaultProps = {
    defaultFirst: false,
    floating: false,
    dependencies: []
}