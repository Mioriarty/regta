import { useState } from "react";

export default function useTrigger() {
    const [ val, setVal ] = useState({});
    return [ () => setVal({}), val];
}