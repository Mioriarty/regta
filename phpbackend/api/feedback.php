<?php

define('ALL_USERS_QUERY', 'SELECT id, CONCAT("{\"name\": \"", name, "\", \"prename\": \"", prename, "\"}") as user, "student" as user_type FROM students
UNION 
SELECT id, CONCAT("{\"title\": \"", title, "\", \"name\": \"", name, "\"}") as user, "teacher" as user_type FROM teachers  
UNION
SELECT id, CONCAT("{\"title\": \"", title, "\", \"name\": \"", name, "\"}") as user, "trainer" as user_type FROM trainers
UNION
SELECT id, CONCAT("{\"name\": \"", name, "\"}") as user, "other" as user_type FROM others');

function registerFeedbackRoutes($app) {

    $app->post('', function ($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        $userType = $req->getAttribute('userType');
        $userId = $body['anonymous'] == 1 ? -1 : $req->getAttribute('userId');

        $createdAt = date('Y-m-d H:i:s');
        $subject = $body['subject'];
        $message = $body['message'];
        $answerMail = $body['answerMail'];
        
        return DB::query("INSERT INTO feedback (sender_id, sender_type, created_at, subject, message, answer_mail) VALUES ('$userId', '$userType', '$createdAt', '$subject', '$message', '$answerMail')", $res, 'DB::CB_POST');
    });

    $app->get('', function ($req, $res, $args) {
        return DB::query("SELECT feedback.id, sender_id as senderId, sender_type as senderType, user as senderInfo, created_at as createdAt, subject, message, answer_mail as answerMail, pinned FROM feedback LEFT JOIN (" . ALL_USERS_QUERY . ") all_users ON all_users.id = feedback.sender_id AND feedback.sender_type = all_users.user_type ORDER BY pinned DESC, created_at DESC", $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other']));

    $app->put('/{feedbackId}/pinned', function ($req, $res, $args) {
        $id = DB::escape($args['feedbackId']);
        $pinned = DB::escape($req->getParsedBody()['pinned']) ? 'true' : 'false';
        return DB::query("UPDATE feedback SET pinned = $pinned WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other']));

    $app->delete('/{feedbackId}', function ($req, $res, $args) {
        $id = DB::escape($args['feedbackId']);
        return DB::query("DELETE FROM feedback WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware(['other']));
}