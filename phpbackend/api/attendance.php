<?php

function generateAllDates($terms, $offset = '+6 days') {
    $last = new DateTime();
    $last->modify($offset);
    $last->setTime(0, 0);

    $q = DB::queryRaw("SELECT date FROM schoolyear WHERE name = 'year_start' OR name = 'year_end' ORDER BY name DESC");
    $yearStart = new DateTime($q->fetch_row()[0]);
    $yearEnd   = new DateTime($q->fetch_row()[0]);

    // dont schow attendances after the year ends
    if($last > $yearEnd)
        $last = $yearEnd;
    

    $weeks  = DB::queryRaw("SELECT week_nr as week, year, ab FROM weeks")->fetch_all(MYSQLI_ASSOC);
    $aWeeks = array_filter($weeks, function ($w) { return $w['ab'] === 'A'; });
    $bWeeks = array_filter($weeks, function ($w) { return $w['ab'] === 'B'; });

    $vacations = DB::queryRaw("SELECT start, end FROM vacations")->fetch_all(MYSQLI_ASSOC);

    $dates = [];
    foreach ($terms as $term) {
        $voluntary = in_array('VOLUNTARY', $term->flags);

        if($term->type === 'once') {
            if($last > new DateTime($term->date)) {
                $dates[] = array("date" => $term->date, "term_id" => $term->uid, "voluntary" => $voluntary);
            }
        } elseif($term->type === 'weekly') {
            $crnt = setDateTimeToPreviousDow(clone $last, $term->dow);
            $isAWeekTerm = in_array('A-WEEK-ONLY', $term->flags);
            $isBWeekTerm = in_array('B-WEEK-ONLY', $term->flags);
            $weeksToCheck = ($isAWeekTerm ? $aWeeks : ($isBWeekTerm ? $bWeeks : []));

            while($crnt >= $yearStart) {
                if($isAWeekTerm || $isBWeekTerm) {
                    if(count(array_filter($weeksToCheck, function($w) use ($crnt) { return $w['week'] == $crnt->format('W') && $w['year'] == $crnt->format('o'); })) == 0) {
                        // is not a-week or not b-week
                        $crnt->modify('-1 week');
                        continue;
                    }
                }

                if(count(array_filter($vacations, function($v) use ($crnt) { return new DateTime($v['start']) <= $crnt && new DateTime($v['end']) >= $crnt ;})) > 0) {
                    // is in a holliday
                    $crnt->modify('-1 week');
                    continue;
                }

                // at crnt date the course takes place
                $dates[] = array("date" => $crnt->format("Y-m-d"), "term_id" => $term->uid, "voluntary" => $voluntary);
                $crnt->modify('-1 week');
            }
        }
    }

    // finally sort them an a descending order
    usort($dates, function($a, $b) {
        if($a['date'] == $b['date']) {
            return 0;
        } else {
            return ($a['date'] < $b['date']) ? 1 : -1;
        }
    });

    return $dates;
}

function setDateTimeToPreviousDow($dateTime, $dow) {
    $firstDayOfWeek = 1; // Monday

    $difference = ($firstDayOfWeek - $dateTime->format('N')) + $dow;
    if ($difference > 0) 
        $difference -= 7;
    $dateTime->modify("$difference days");

    return $dateTime;
}

function registerAttendanceRoutes($app) {

    // ******************
    // ****** GET *******
    // ******************

    $app->get('/courses/{id:[0-9]+}/{term:all|once|[0-9]+}', function ($req, $res, $args) {
        $courseId = DB::escape($args['id']);
        $term = DB::escape($args['term']);

        // Get students that take part in that term
        if($term=== 'all' || $term === 'once') {
            $studentsQuery = DB::queryRaw("SELECT id, name, prename, visit_voluntary FROM students_in_courses INNER JOIN students ON student_id = id WHERE course_id = '$courseId'");
        } else {
            $studentsQuery = DB::queryRaw("SELECT id, name, prename, visit_voluntary FROM students_in_courses INNER JOIN students ON student_id = id WHERE course_id = '$courseId' AND visit_voluntary LIKE '%$term%'");
        }
        $students = [];
        while($s = $studentsQuery->fetch_assoc()) {
            $s['visit_voluntary'] = json_decode($s['visit_voluntary']);
            $students[] = $s;
        }

        // get necessary term info
        $terms = DB::queryRaw("SELECT schedule FROM courses WHERE id = '$courseId'");
        if($terms->num_rows === 0) {
            $res->getBody()->write(json_encode(array("error" => "course not found")));
            return $res->withStatus(404);
        }

        $terms = json_decode($terms->fetch_row()[0]);
        if($term === 'all') {
            // take all terms
        } elseif($term === 'once') {
            $terms = array_filter($terms, function ($e) { return $e->type === 'once';});
        } else {
            $terms = array_filter($terms, function ($e) use ($term) { return $e->uid == $term;});
        }

        $dates = generateAllDates($terms);

        $attendances = DB::queryRaw("SELECT * FROM attendances WHERE course_id = '$courseId'")->fetch_all(MYSQLI_ASSOC);

        $res->getBody()->write(json_encode(array("students" => $students, "dates" => $dates, "attendances" => $attendances)));
        return $res;
    });


    $app->get('/students/{student-id}', function($req, $res, $args) {
        $sId = DB::escape($args['student-id']);
        $attendances = DB::queryRaw("SELECT * FROM attendances WHERE student_id = $sId")->fetch_all(MYSQLI_ASSOC);
        
        $q = DB::queryRaw("SELECT DISTINCT course_id, visit_voluntary, schedule FROM students_in_courses INNER JOIN courses ON courses.id = course_id WHERE student_id = '$sId'");
        $courses = array();

        while($c = $q->fetch_assoc()) {
            $cId = $c['course_id'];
            $visitVoluntary = json_decode($c['visit_voluntary']);
            $terms = json_decode($c['schedule']);
            $terms = array_filter($terms, function ($t) use ($visitVoluntary) { return !in_array('VOLUNTARY', $t->flags) || in_array($t->uid, $visitVoluntary); });

            $courses[$cId] = array("studentVisitsVoluntary" => $visitVoluntary, "dates" => generateAllDates($terms));
        }

        $res->getBody()->write(json_encode(array("courses" => $courses, "attendances" => $attendances)));
        return $res;
    });


    // ******************
    // ****** PUT *******
    // ******************
    

    $app->put('', function ($req, $res, $args) {
        $body = $req->getParsedBody();
        $userType = $req->getAttribute('userType');
        $userId = $req->getAttribute('userId');

        $atts = [];
        if($userType === 'other') {
            // all attendance changes are allowed
            $atts = $body;
        } elseif($userType === 'trainer') {
            // only attendance changes of their own courses are allowed
            $q = DB::queryRaw("SELECT id FROM courses WHERE trainer_id = '$userId'");
            $validCourseIds = [];
            while($cid = $q->fetch_row())
                $validCourseIds[] = $cid[0];
            
            foreach ($body as $a) {
                if(in_array($a['course_id'], $validCourseIds))
                    $atts[] = $a;
            }
        } elseif($userType === 'teacher') {
            // only attendance changes of students in their class are allowed
            $q = DB::queryRaw("SELECT students.id FROM students INNER JOIN teachers ON students.class = teachers.class WHERE teachers.id = '$userId'");
            $validStudentIds = [];
            while($sid = $q->fetch_row())
                $validStudentIds[] = $sid[0];
            
            foreach ($body as $a) {
                if(in_array($a['course_id'], $validStudentIds))
                    $atts[] = $a;
            }
        }
        if(count($atts) === 0) {
            $res->getBody()->write(json_encode(array("status" => "ok")));
            return $res;
        }

        $values = [];
        foreach ($atts as $a) {
            $a = DB::escapeArray($a);
            $values[] = "('{$a['student_id']}', '{$a['course_id']}', '{$a['term_id']}', '{$a['date']}', '{$a['status']}', '{$a['comment']}')";
        }
        $values = implode(', ', $values);

        return DB::query("INSERT INTO attendances VALUES $values ON DUPLICATE KEY UPDATE status = VALUES(status), comment = VALUES(comment)", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other', 'teacher', 'trainer']));

    // ******************
    // ***** COUNTS *****
    // ******************
    // used for these little ratio bars

    $app->get('/counts/{person:students|trainers}/{person-id:[0-9]+}[/courses/{course-id:[0-9]+}]', function($req, $res, $args) {
        $person   = DB::escape($args['person']);
        $personId = DB::escape($args['person-id']);

        $courseSpecified = isset($args['course-id']);
        if($courseSpecified)
            $courseId = DB::escape($args['course-id']);
        
        if($person === 'students') {
            $q = DB::queryRaw("SELECT status, COUNT(*) as count FROM attendances WHERE (NOT status = 0) AND student_id = '$personId'" . ($courseSpecified ? " AND course_id = $courseId" : '') . ' GROUP BY status');
        } elseif($person === 'trainers') {
            $q = DB::queryRaw("SELECT id FROM courses WHERE trainer_id = '$personId'");
            $possibleCourseIds = [];
            while($id = $q->fetch_row())
                $possibleCourseIds[] = $id[0];
            
            if($courseSpecified) {
                if(in_array($courseId, $possibleCourseIds)) {
                    $possibleCourseIds = [ $courseId ];
                } else {
                    $possibleCourseIds = [];
                }
            }

            if(count($possibleCourseIds) == 0) {
                $res->getBody()->write(json_encode(array(1 => 0, 2 => 0, 3 => 0)));
                return $res;
            }

            $whereCondition = implode(' OR ', array_map(function ($id) {return "course_id = '$id'";}, $possibleCourseIds));
            
            $q = DB::queryRaw("SELECT status, COUNT(*) as count FROM attendances WHERE (NOT status = 0) AND student_id = '-1' AND ($whereCondition) GROUP BY status");
        }

        $resJson = array(1 => 0, 2 => 0, 3 => 0);
        while($row = $q->fetch_assoc())
            $resJson[intval($row['status'])] = intval($row['count']);

        $res->getBody()->write(json_encode($resJson));
        return $res;
    });

}