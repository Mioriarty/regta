<?php

define("COUNT_COURSE_SELECTION_TRIES", 50);

function isCourseSelectionHappening() {
    $q = DB::queryRaw("SELECT date FROM schoolyear WHERE name = 'course_selection_start' OR name = 'course_selection_end' ORDER BY name DESC");
    $now = new DateTime();
    $now->setTime(0, 0);
    return new DateTime($q->fetch_row()[0]) <= $now && new DateTime($q->fetch_row()[0]) >= $now;
}

function executeOneCourseSelectionTry($maxAllocationsForCourses, $allocationsLeftForStudents) {
    $assignedStudents = array_map(function($e) { return []; }, $maxAllocationsForCourses); // map: course_id => students_that_got_allocated_to_that_course
    $studentsThatGotACourse = [];
    // This query randomises the selections within priority levels and filters the selections where the student is not in the course yet
    $q = DB::queryRaw("SELECT student_id, course_id FROM `course_selection_courses` WHERE (SELECT COUNT(*) FROM students_in_courses WHERE students_in_courses.student_id = course_selection_courses.student_id AND students_in_courses.course_id = course_selection_courses.course_id) = 0 ORDER BY priority, RAND()");
    
    while($selection = $q->fetch_assoc()) {
        // if there is still space in course
        if(count($assignedStudents[$selection['course_id']]) < $maxAllocationsForCourses[$selection['course_id']]) {
            // if student still wants to get courses
            if($allocationsLeftForStudents[$selection['student_id']] > 0) {
                $assignedStudents[$selection['course_id']][] = $selection['student_id'];
                $studentsThatGotACourse[] = $selection['student_id'];
                $allocationsLeftForStudents[$selection['student_id']] -= 1;
            }
        }
    }

    return [ $assignedStudents, $studentsThatGotACourse ];
}

function computeDissatisfactionScore($studentsThatGotACourse, $numSelectedCoursesByStudents) {
    // 5 points for a student that didnt get any course
    // 1 additional points for the number of every course they whished for

    // remove every student that got a course, duplicates don't matter
    foreach ($studentsThatGotACourse as $studentId)
        unset($numSelectedCoursesByStudents[$studentId]);
    
    // for every who is left, accumalate the dissatisfaction
    $dissatisfaction = 0;
    foreach ($numSelectedCoursesByStudents as $numSelectedCourses) {
        $dissatisfaction += 5 + $numSelectedCourses;
    }

    return $dissatisfaction;
}

function registerCourseSelectionRoutes($app) {

    $app->get('/studentInfo/{studentId}', function($req, $res, $args) {
        $studentId = DB::escape($args['studentId']);
        $userType  = $req->getAttribute('userType');
        
        if($userType == 'student') {
            if($req->getAttribute('userId') != $studentId)
                throw new HttpNotAuthorizedException();

            if(!isCourseSelectionHappening()) {
                $res->getBody()->write(json_encode(array('error' => 'course selection is not happening currently')));
                return $res;
            }
        }

        // get allready allocated courses
        $q = DB::queryRaw("SELECT course_id FROM students_in_courses WHERE student_id = '$studentId'");
        $allocatedCourses = [];
        while($row = $q->fetch_row())
            $allocatedCourses[] = $row[0];

        // get priority courses
        $q = DB::queryRaw("SELECT course_id FROM student_priority_courses WHERE student_id = '$studentId'");
        $priorityCourses = [];
        while($row = $q->fetch_row())
            $priorityCourses[] = $row[0];
        
        // get already selected courses
        $q = DB::queryRaw("SELECT courses.id, priority, courses.title, trainers.title as trainer_title, trainers.name as trainer_name FROM course_selection_courses INNER JOIN courses ON course_id = courses.id INNER JOIN trainers ON trainer_id = trainers.id WHERE student_id = '$studentId' ORDER BY priority");
        $selectedCourses = [];
        while($row = $q->fetch_assoc())
            $selectedCourses[] = $row;
        
        // get selected max allocated courses
        $q = DB::queryRaw("SELECT max_allocation FROM course_selection_max_allocations WHERE student_id = '$studentId'");
        $maxAllocations = $q->num_rows == 0 ? 1 : $q->fetch_row()[0];

        $res->getBody()->write(json_encode(array('allocatedCourses' => $allocatedCourses, 'priorityCourses' => $priorityCourses, 'selectedCourses' => $selectedCourses, 'maxAllocations' => $maxAllocations)));
        return $res;

    });

    $app->post('/submit', function($req, $res, $args) {
        $studentId = DB::escape($req->getAttribute('userId'));
        $body = $req->getParsedBody();

        if(!isCourseSelectionHappening())
            throw new HttpNotAuthorizedException();
        

        // check if student exists and get class level of the student
        $q = DB::queryRaw("SELECT class FROM students WHERE id = '$studentId'");
        if($q->num_rows === 0)
            throw new HttpNotAuthorizedException();
        $classLevel = explode('/', $q->fetch_row()[0])[0];

        // get valid priority courses for later checking
        $priorityCourses = [];
        $q = DB::queryRaw("SELECT course_id FROM student_priority_courses WHERE student_id = '$studentId'");
        while($row = $q->fetch_row())
            $priorityCourses[] = $row[0];
        
        // get all allowed class levels for the selected courses
        $whereStm = implode(' OR ', array_map(function($v) { return "id = '{$v['id']}'"; }, $body['selectedCourses']));
        $q = DB::queryRaw("SELECT id, class_levels FROM courses WHERE $whereStm");
        
        // prepare insert stm while checking class levels
        $values = [];
        while($c = $q->fetch_assoc()) {
            if(in_array($classLevel, explode('|', $c['class_levels']))) {
                // get and check prority
                $priority = current(array_filter($body['selectedCourses'], function($v) use ($c) { return $c['id'] == $v['id']; }))['priority'];
                if($priority < 1) {
                    if(in_array($c['id'], $priorityCourses)) {
                        $priority = 0;
                    } else {
                        continue;
                    }
                }
                if($priority > 4) $priority = 4;

                $values[] = "('$studentId', '{$c['id']}', '$priority')";
            }
        }
        $values = implode(', ', $values);

        // clear course selection
        DB::queryRaw("DELETE FROM course_selection_courses WHERE student_id = '$studentId'");

        // insert course selection
        DB::queryRaw("INSERT INTO course_selection_courses VALUES $values");

        // set max allocations
        $maxAllocations = DB::escape($body['maxAllocations']);
        $maxAllocations = max(1, min(3, $maxAllocations));
        DB::queryRaw("INSERT INTO course_selection_max_allocations VALUES ('$studentId', '$maxAllocations') ON DUPLICATE KEY UPDATE max_allocation = VALUES(max_allocation)");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;

    })->add(genAuthMiddleware('student'));

    $app->get('/status', function($req, $res, $args) {
        $q = DB::queryRaw("SELECT date FROM schoolyear WHERE name = 'course_selection_start' OR name = 'course_selection_end' ORDER BY name DESC");
        $start = $q->fetch_row()[0];
        $end = $q->fetch_row()[0];

        $numParticipants = DB::queryRaw("SELECT COUNT(DISTINCT student_id) FROM course_selection_courses")->fetch_row()[0];
        $totalStudents = DB::queryRaw('SELECT COUNT(*) FROM students')->fetch_row()[0];

        $res->getBody()->write(json_encode(array('start' => $start, 'end' => $end, 'numParticipants' => $numParticipants, 'totalStudents' => $totalStudents)));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->post('/convertVisitingToPriorityCourses', function($req, $res, $args) {
        // check if there are even students in courses, so that it cannot happen that all data is lost
        $count = DB::queryRaw("SELECT COUNT(*) FROM students_in_courses")->fetch_row()[0];
        if($count == 0) {
            $res->getBody()->write(json_encode(array("status" => "error", "message" => "Es befinden sich keine Schüler in Kursen. Möglicherweise wurde die Operation schonmal durchgeführt. Vorgang abgebrochen")));
            return $res;
        }

        DB::createBackup("Dieses Backup wurde automatisch erstellt, bevor im Zuge der Vorbereitung einer Kurswahl, die ehemalig besuchten Kurse von Schüler gelöscht und als Prioritäts-Kurse in der Kurswahl umgetragen wurden.", true);

        // execute
        DB::queryRaw("TRUNCATE TABLE student_priority_courses");
        DB::queryRaw("INSERT INTO student_priority_courses (student_id, course_id) SELECT student_id, course_id FROM students_in_courses");
        DB::queryRaw("TRUNCATE TABLE students_in_courses");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->post('/execute', function($req, $res, $args) {
        if(isCourseSelectionHappening())
            throw new HttpNotAuthorizedException();

        // prepare max allocations by course into map: course_id => max_allocations
        $q = DB::queryRaw("SELECT id, size, (SELECT COUNT(student_id) FROM students_in_courses WHERE course_id = courses.id) as count_students FROM courses");
        $maxAllocationsForCourses = [];
        while($row = $q->fetch_assoc())
            $maxAllocationsForCourses[$row['id']] = $row['size'] - $row['count_students'];
        
        // prepare max allocations for students into map: student_id => max_allocations
        $q = DB::queryRaw('SELECT * FROM `course_selection_max_allocations`');
        $maxAllocationsForStudents = [];
        while($row = $q->fetch_row())
            $maxAllocationsForStudents[$row[0]] = $row[1];
            
        
        // prepare num of selected courses into map: student_id => num_selected_courses, ignore wishes where strudent os already in the course
        $q = DB::queryRaw("SELECT student_id, COUNT(course_id) FROM `course_selection_courses` WHERE (SELECT COUNT(*) FROM students_in_courses WHERE students_in_courses.student_id = course_selection_courses.student_id AND students_in_courses.course_id = course_selection_courses.course_id) = 0 GROUP BY student_id");
        $numSelectedCoursesByStudents = [];
        while($row = $q->fetch_row())
            $numSelectedCoursesByStudents[$row[0]] = $row[1];
        
        // now try a lot of assignments and select the one with lowest dissatisfaction score 
        $minDissatisfactionScore = PHP_INT_MAX;
        $bestStudentAssignment = [];

        for($i = 0; $i < COUNT_COURSE_SELECTION_TRIES; $i++) {
            [ $assignedStudents, $studentsThatGotACourse ] = executeOneCourseSelectionTry($maxAllocationsForCourses, $maxAllocationsForStudents);
            $dissatisfactionScore = computeDissatisfactionScore($studentsThatGotACourse, $numSelectedCoursesByStudents);

            if($dissatisfactionScore < $minDissatisfactionScore) {
                $minDissatisfactionScore = $dissatisfactionScore;
                $bestStudentAssignment = $assignedStudents;
            }
        }

        // now enter best assignment in database
        $values = [];
        foreach ($bestStudentAssignment as $courseId => $students) {
            foreach ($students as $studentId) {
                $values[] = "('$courseId', '$studentId', '[]')";
            }
        }
        $numAllocations = count($values);
        $values = implode(', ', $values);

        DB::createBackup("Dieses Backup wurde automatisch nach der Wahlperiode der Kurswahl aber vor dem Zurdnen der Schüler in ihre gewählten Kurse erstellt.", true);

        DB::queryRaw("INSERT INTO students_in_courses (course_id, student_id, visit_voluntary) VALUES $values");

        // remove all wanted courses
        DB::queryRaw("TRUNCATE TABLE course_selection_courses");
        DB::queryRaw("TRUNCATE TABLE course_selection_max_allocations");

        $res->getBody()->write(json_encode(array("status" => "ok", "dissatisfactionScore" => $minDissatisfactionScore, "numAllocations" => $numAllocations)));
        return $res;
    })->add(genAuthMiddleware('other'));
}