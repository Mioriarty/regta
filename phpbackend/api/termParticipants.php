<?php

function registerTermParticipantsRoutes($app) {

    $app->get('/{courseId}', function ($req, $res, $args) {
        $courseId = DB::escape($args['courseId']);
        $course = DB::queryRaw("SELECT * FROM courses WHERE id = '$courseId'")->fetch_assoc();
        if($req->getAttribute('userType') === 'trainer') {
            $trainerId = $req->getAttribute('userId');
            if($course['trainer_id'] != $trainerId) {
                // Course is not led by that trainer
                throw new HttpNotAuthorizedException();
            }
        }
        
        $students = DB::queryRaw("SELECT student_id, visit_voluntary, prename, name, class FROM students_in_courses INNER JOIN students ON student_id = id WHERE course_id = '$courseId'")->fetch_all(MYSQLI_ASSOC);
        $course['students'] = $students;

        $res->getBody()->write(json_encode($course));
        return $res;
    });

    $app->put('/{courseId}', function ($req, $res, $args) {
        $courseId = DB::escape($args['courseId']);
        if($req->getAttribute('userType') === 'trainer') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT trainer_id FROM courses WHERE id = '$courseId'")->num_rows === 0) {
                // Course is not led by that trainer
                throw new HttpNotAuthorizedException();
            }
        }
        $body = $req->getParsedBody();
        foreach ($body as $studentId => $terms) {
            for($i = 0; $i < count($terms); $i++)
                $terms[$i] = (int) DB::escape($terms[$i]);
            DB::queryRaw("UPDATE students_in_courses SET visit_voluntary = '" . json_encode($terms) . "' WHERE student_id = '$studentId' AND course_id = '$courseId'");
        }
        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    });
}