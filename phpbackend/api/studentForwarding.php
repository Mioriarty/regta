<?php

function registerStudentForwardingRoutes($app) {
    $app->get('/{grade}/{classNr}', function ($req, $res, $args) {
        $class = DB::escape($args['grade'] . '/' . $args['classNr']);
        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT * FROM courses WHERE id = '$courseId' AND `class` = '$class'")->num_rows === 0) {
                // It's not that teacher's class
                throw new HttpNotAuthorizedException();
            }
        }

        $q = DB::queryRaw("SELECT id, prename, name, COALESCE(`target`,'no info') as forwarding_target FROM students LEFT JOIN student_forwarding ON id = student_id WHERE `class`= '$class' ORDER BY name, prename");
        $oldStudentInfos = $q->fetch_all(MYSQLI_ASSOC);

        $nextClass = sprintf('%02d', ((int) $args['grade']) + 1) . '/' . $args['classNr'];

        $q = DB::queryRaw("SELECT * FROM student_forwarding_new_students WHERE `class`= '$nextClass'");
        $newStudentInfos = $q->fetch_all(MYSQLI_ASSOC);

        $q = DB::queryRaw("SELECT id, prename, name, `class` FROM students INNER JOIN student_forwarding ON id = student_id WHERE `target` = '$nextClass' AND NOT `class` = '$class'");
        $leanStudentInfos = $q->fetch_all(MYSQLI_ASSOC);

        $res->getBody()->write(json_encode(array("oldStudents" => $oldStudentInfos, "newStudents" => $newStudentInfos, "leanStudents" => $leanStudentInfos)));
        return $res;
    });

    $app->put('/{grade}/{classNr}', function ($req, $res, $args) {
        $body = $req->getParsedBody();
        $class = DB::escape($args['grade'] . '/' . $args['classNr']);
        $nextClass = sprintf('%02d', ((int) $args['grade']) + 1) . '/' . $args['classNr'];

        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT * FROM courses WHERE id = '$courseId' AND `class` = '$class'")->num_rows === 0) {
                // It's not that teacher's class
                throw new HttpNotAuthorizedException();
            }
        }
        
        // Old students
        $studentsWithTarget = array_filter($body['oldStudents'], function($s) { return $s['target'] != 'no info' && $s['target'] != ''; });
        $studentsWithoutTarget = array_filter($body['oldStudents'], function($s) { return $s['target'] == 'no info' || $s['target'] == ''; });
        $malformattedTargetFound = false;

        $studentsWithTarget = array_map(function($s) use ($malformattedTargetFound) {
            $id = DB::escape($s['id']);
            $target = DB::escape($s['target']);
            if($target != 'leave' && preg_match_all('#[0-9][0-9]/[1-9]#', $target) != 1)
                $malformattedTargetFound = true;
            return "('$id', '$target')";
        }, $studentsWithTarget );
        $studentsWithoutTarget = array_map(function($s) {
            $id = DB::escape($s['id']);
            return "student_id = '$id'"; 
        }, $studentsWithoutTarget );

        if($malformattedTargetFound) {
            $res = $response->withStatus(500);
			$res->getBody()->write(json_encode(array("error" => "Invalid class submitted", "status" => "failed")));
			return $res;
        }

        $studentsWithTarget = implode(', ', $studentsWithTarget);
        $studentsWithoutTarget = implode(' OR ', $studentsWithoutTarget);

        DB::queryRaw("DELETE FROM student_forwarding WHERE `target` = '$nextClass'");
        DB::queryRaw("INSERT INTO student_forwarding VALUES $studentsWithTarget ON DUPLICATE KEY UPDATE `target` = VALUES(`target`)" );
        DB::queryRaw("DELETE FROM student_forwarding WHERE $studentsWithoutTarget");

        // New students
        DB::queryRaw("DELETE FROM student_forwarding_new_students WHERE `class` = '$nextClass'");
        
        $newStudents = array_map(function($s) use ($nextClass) {
            $name = DB::escape($s['name']);
            $prename = DB::escape($s['prename']);
            return "('$prename', '$name', '$nextClass')";
        }, $body['newStudents']);

        if(count($newStudents) > 0) {
            $newStudents = implode(', ', $newStudents);
            DB::queryRaw("INSERT INTO student_forwarding_new_students VALUES $newStudents");
        }
        

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    });

    $app->put('/uploadCsv', function ($req, $res, $args) {
        $studentForwardingValues = [];
        $newStudentsValues = [];

        $countSuccessfulMatches = 0;
        $countNewStudents = 0;
        $countUncertainCases = 0;

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $req->getBody()->getContents()) as $line){
            $data = str_getcsv($line, ';');
            
            if(count($data) < 3)
                continue;

            $name = DB::escape(trim($data[0]));
            $prename = DB::escape(trim($data[1]));
            $class = DB::escape(trim($data[2]));
            
            if(strlen($class) == 3)
                $class = '0' . $class;
            
            if(strlen($class) != 4 || preg_match("/([0-1][0-9])\/[1-9]/", $class) == 0)
                continue;
            
            $data = DB::escapeArray($data);
            
            $q = DB::queryRaw("SELECT id FROM students WHERE name = '{$name}' AND prename = '{$prename}'");
            if($q->num_rows == 1) {
                // perfect match
                $studentForwardingValues[] = "('{$q->fetch_row()[0]}', '{$class}')";
                $countSuccessfulMatches++;
            } else if($q->num_rows > 1) {
                // 2 students with exact same name
                $countUncertainCases++;
            } else {
                // no exact student found, try with if strings contain one another
                $q = DB::queryRaw("SELECT id FROM students WHERE (INSTR(prename, '$prename') OR INSTR('$prename', prename)) AND (INSTR(name, '$name') OR INSTR('$name', name))");

                if($q->num_rows == 1) {
                    // perfect match
                    $studentForwardingValues[] = "('{$q->fetch_row()[0]}', '{$class}')";
                    $countSuccessfulMatches++;
                } else if($q->num_rows > 1) {
                    // 2 students with exact same name
                    $countUncertainCases++;
                } else {
                    $newStudentsValues[] = "('{$name}', '{$prename}', '{$class}')";
                    $countNewStudents++;
                }
            }
        }
        $studentForwardingValues = implode(', ', $studentForwardingValues);
        $newStudentsValues = implode(', ', $newStudentsValues);

        DB::queryRaw("INSERT INTO student_forwarding (student_id, target) VALUES $studentForwardingValues ON DUPLICATE KEY UPDATE `target` = VALUES(`target`)");
        DB::queryRaw("INSERT IGNORE INTO student_forwarding_new_students (name, prename, `class`) VALUES $newStudentsValues");

        $res->getBody()->write(json_encode(array(
            'status' => 'ok',
            'processedRows' => $countSuccessfulMatches + $countNewStudents + $countUncertainCases,
            'successfulMatches' => $countSuccessfulMatches, 
            'newStudents' => $countNewStudents, 
            'uncertainCases' => $countUncertainCases
        )));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->get('/ready', function ($req, $res, $args) {
        $q = DB::queryRaw('SELECT target FROM students LEFT JOIN student_forwarding ON student_id = id WHERE target IS NULL');
        $resObj = array("ready" => $q->num_rows == 0);
        if(!$resObj['ready']) {
            $resObj['unsetStudents'] = $q->num_rows;

            // Select random class with unset students
            $randomClass = DB::queryRaw('SELECT `class` FROM students LEFT JOIN student_forwarding ON student_id = id WHERE target IS NULL ORDER BY RAND() LIMIT 1')->fetch_assoc()['class'];
            $resObj['randomClassWithUnsetStudents'] = $randomClass;
        }

        $res->getBody()->write(json_encode($resObj));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->get('/preview', function ($req, $res, $args) {
        // Compile all teachers
        $teachers = [];
        $classes = [];
        $q = Db::queryRaw("SELECT class, title, name FROM teachers ORDER BY class");
        while($teacher = $q->fetch_assoc()) {
            $splittedClass = explode("/", $teacher['class']);
            $teacher['class'] = sprintf('%02d', ((int) $splittedClass[0]) + 1) . '/' . $splittedClass[1];
            
            $teachers[] = $teacher;
            $classes[] = $teacher['class'];
        }

        // Compile all students
        $q = DB::queryRaw("SELECT *, 'new' as origin FROM student_forwarding_new_students UNION SELECT students.prename, students.name, target as class, students.class as origin FROM student_forwarding INNER JOIN students ON student_id = id WHERE NOT target = 'leave' UNION SELECT students.prename, students.name, students.class, 'no info' as origin FROM students LEFT JOIN student_forwarding ON student_id = id WHERE target IS NULL ORDER BY class, name, prename");
        $crntInsertingClass = '';
        $students = [];
        while($student = $q->fetch_assoc()) {
            if($crntInsertingClass != $student['class']) {
                $crntInsertingClass = $student['class'];
                $students[$crntInsertingClass] = [];

                // if we dont have a teacher for that class, add dummy teacher
                if(!in_array($crntInsertingClass, $classes)){
                    $teachers[] = array('class' => $crntInsertingClass, 'title' => '*Ohne', 'name' => 'Klassenleiter*');
                }
            }
            
            unset($student['class']);
            $students[$crntInsertingClass][] = $student;
        }

        $res->getBody()->write(json_encode(array("teachers" => $teachers, "students" => $students)));
        return $res;

    })->add(genAuthMiddleware('other'));

    $app->put('/execute', function ($req, $res, $args) {
        DB::createBackup("Dieses Backup wurde automatisch erstellt, bevor sämtliche Schüler gemäß der Informationen der Seite \"Schuljahreswechsel für Schüler vorbereiten\" in ihre nächsten Klassen verschoben wurden.", true);

        // Deletions
        $q = DB::queryRaw("SELECT student_id FROM student_forwarding WHERE target = 'leave'");
        $whereClause = [];
        while($studentId = $q->fetch_assoc()['student_id']) {
            $whereClause[] = "id = '$studentId'";
        }
        $whereClause = implode(' OR ', $whereClause);
        DB::queryRaw("DELETE FROM students WHERE $whereClause");

        // Forwardings
        $q = DB::queryRaw("SELECT * FROM student_forwarding WHERE NOT target = 'leave'");
        $values = [];
        while($row = $q->fetch_assoc()) {
            $values[] = "('{$row['student_id']}', '{$row['target']}')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO students (id, `class`) VALUES $values ON DUPLICATE KEY UPDATE name=name, prename=prename, pwd=pwd, `class`=VALUES(`class`)");

        // New Students
        $q = DB::queryRaw("SELECT * FROM student_forwarding_new_students");
        $values = [];
        while($student = $q->fetch_assoc()) {
            $pwd = DB::genSimplePassword();
            $values[] = "('{$student['prename']}', '{$student['name']}', '{$student['class']}', '$pwd')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO students (prename, name, `class`, pwd) VALUES $values");

        // Raise Teachers
        $q = DB::queryRaw("SELECT id, `class` FROM teachers");
        $values = [];
        while($teacher = $q->fetch_assoc()) {
            $splittedClass = explode("/", $teacher['class']);
            $futureClass = sprintf('%02d', ((int) $splittedClass[0]) + 1) . '/' . $splittedClass[1];
            $values[] = "('{$teacher['id']}', '$futureClass')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO teachers (id, `class`) VALUES $values ON DUPLICATE KEY UPDATE name=name, title=title, pwd=pwd, `class`=VALUES(`class`)");

        // Dangling classes
        $defaultPwd = 'lehrer';
        $defaultName = 'Unbekannt';
        $q = Db::queryRaw("SELECT DISTINCT students.class as class FROM students LEFT JOIN teachers ON students.class = teachers.class WHERE teachers.class IS NULL");
        $values = [];
        while($class = $q->fetch_assoc()['class']) {
            $title = (['Herr', 'Frau'])[array_rand(range(0, 1))];
            $pwd = password_hash($defaultPwd, PASSWORD_DEFAULT);
            $values[] = "('$title', '$defaultName', '$pwd', '$class')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO teachers (title, name, pwd, `class`) VALUE $values");
        
        // Remove all forwarding information
        DB::queryRaw("TRUNCATE TABLE student_forwarding");
        DB::queryRaw("TRUNCATE TABLE student_forwarding_new_students");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->put('/reset', function ($req, $res, $args) {
        DB::createBackup("Dieses Backup wurde automatisch erstellt, bevor die Daten der Seite \"Schuljahreswechsel für Schüler vorbereiten\" zurückgesetzt wurden.", true);

        DB::queryRaw("TRUNCATE TABLE student_forwarding");
        DB::queryRaw("TRUNCATE TABLE student_forwarding_new_students");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    })->add(genAuthMiddleware('other'));
}