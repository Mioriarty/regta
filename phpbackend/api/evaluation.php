<?php

function registerEvaluationManagementRoutes($app) {
    $app->get('', function($req, $res, $args) {
        return DB::query('SELECT *, (SELECT COUNT(*) FROM evaluation_answers WHERE evaluation_id = id) AS count_answers, (SELECT COUNT(*) FROM evaluation_participants WHERE evaluation_id = id) AS count_participants FROM evaluations', $res, 'DB::CB_GET');
    });

    $app->post('', function ($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());

        $hasPeriod = $body['periodStart'] != '' && $body['periodEnd'] != '';
        $periodStart = $hasPeriod ? "'{$body['periodStart']}'" : 'NULL';
        $periodEnd = $hasPeriod ? "'{$body['periodEnd']}'" : 'NULL';
        return DB::query("INSERT INTO evaluations (title, target_group, period_start, period_end, `data`) VALUES ('{$body['title']}', '{$body['targetGroup']}', $periodStart, $periodEnd, '{$body['data']}')", $res, 'DB::CB_POST');
    });

    $app->put('/{id:[0-9]+}', function($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        $id   = DB::escape($args['id']);

        $hasPeriod = $body['periodStart'] != '' && $body['periodEnd'] != '';
        $periodStart = $hasPeriod ? "'{$body['periodStart']}'" : 'NULL';
        $periodEnd = $hasPeriod ? "'{$body['periodEnd']}'" : 'NULL';
        return DB::query("UPDATE evaluations SET title = '{$body['title']}', target_group = '{$body['targetGroup']}', period_start = $periodStart, period_end = $periodEnd,  `data` = '{$body['data']}' WHERE id = '$id'", $res, 'DB::CB_PUT');
    });

    $app->delete('/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        DB::queryRaw("DELETE FROM evaluation_participants WHERE evaluation_id = '$id'");
        DB::queryRaw("DELETE FROM evaluation_answers WHERE evaluation_id = '$id'");
        return DB::query("DELETE FROM evaluations WHERE id = '$id'", $res, 'DB::CB_DELETE');
    });

    $app->delete('/{id:[0-9]+}/answers', function($req, $res, $args) {
        $id = DB::escape($args['id']);
        
        DB::queryRaw("DELETE FROM evaluation_participants WHERE evaluation_id = '$id'");
        return DB::query("DELETE FROM evaluation_answers WHERE evaluation_id = '$id'", $res, 'DB::CB_DELETE');
    });
}


function registerEvaluationQuestionnaireRoutes($app) {

    $app->get('/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        $eval = DB::queryRaw("SELECT title, target_group, period_start, period_end, `data` FROM evaluations WHERE id = '$id'");
        if($eval->num_rows == 0) {
            $res = $res->withStatus(404);
            $res->getBody()->write(json_encode(array("error" => "Requested evaulation questionnaire was not found")));
            return $res;
        }


        $eval = $eval->fetch_assoc();
        $userType = $req->getAttribute('userType');
        if($userType != 'other' && $userType != $eval['target_group']) {
            throw new HttpNotAuthorizedException(); // person is not allowed to answer this questionnaire
        }

        // check period
        $now = new DateTime();
        if($userType != 'other' && (new DateTime($eval["period_start"]) > $now || new DateTime($eval["period_end"]) < $now)) {
            throw new HttpNotAuthorizedException();
        }
        
        // get the courses the student attends or the trainer leads
        $eval["courses"] = [];
        $userId = $req->getAttribute('userId');
        if($userType == "student") {
            $courses = DB::queryRaw("SELECT course_id as id, courses.title as title, trainers.title as trainer_title, trainers.name as trainer_name FROM students_in_courses INNER JOIN courses ON course_id = courses.id INNER JOIN trainers ON trainer_id = trainers.id WHERE student_id = '$userId'");
            $eval["courses"] = $courses->fetch_all(MYSQLI_ASSOC);
        } elseif($userType == "trainer") {
            $courses = DB::queryRaw("SELECT id, title FROM courses WHERE trainer_id = '$userId'");
            $eval["courses"] = $courses->fetch_all(MYSQLI_ASSOC);
        }

        $res->getBody()->write(json_encode($eval));
        return $res;
    });

    $app->post('/{id:[0-9]+}/submit', function($req, $res, $args) {
        // others should not participate
        $userType = $req->getAttribute('userType');
        if($userType == 'other') {
            throw new HttpNotAuthorizedException(); 
        }

        $id = DB::escape($args['id']);

        $eval = DB::queryRaw("SELECT title, target_group, period_start, period_end, `data` FROM evaluations WHERE id = '$id'");
        if($eval->num_rows == 0) {
            $res = $res->withStatus(404);
            $res->getBody()->write(json_encode(array("error" => "Requested evaulation questionnaire was not found")));
            return $res;
        }

        $eval = $eval->fetch_assoc();
        $userType = $req->getAttribute('userType');
        if($userType != 'other' && $userType != $eval['target_group']) {
            throw new HttpNotAuthorizedException(); // person is not allowed to answer this questionnaire
        }

        // check period
        $now = new DateTime();
        if(new DateTime($eval["period_start"]) > $now || new DateTime($eval["period_end"]) < $now) {
            throw new HttpNotAuthorizedException();
        }

        $userId = $req->getAttribute('userId');

        // check if they have already answered
        if(DB::queryRaw("SELECT * FROM evaluation_participants WHERE evaluation_id = '$id' AND user_id = '$userId'")->num_rows > 0) {
            throw new HttpNotAuthorizedException();
        }

        // save participation
        DB::queryRaw("INSERT INTO evaluation_participants (evaluation_id, user_id) VALUES ('$id', '$userId')");

        // now actually do the submition
        $body = $req->getParsedBody();
        $values = [];
        foreach ($body as $entry) {
            if($entry[3] != '') {
                $entry = DB::escapeArray($entry);
                $values[] = "('$id', '{$entry[0]}', '{$entry[1]}', '{$entry[2]}', '{$entry[3]}')";
            }
        }
        $values = implode(', ', $values);

        return DB::query("INSERT INTO evaluation_answers (evaluation_id, section_index, question_index, course_id, answer) VALUES $values", $res, 'DB::CB_PUT');
    });

    $app->get('/available', function($req, $res, $args) {
        $userType = $req->getAttribute('userType');

        if($userType == 'other') {
            return DB::query("SELECT id, title FROM evaluations", $res, 'DB::CB_GET');
        }

        $userId = $req->getAttribute('userId');
        $now = date("Y-m-d H:i:s", (new DateTime())->format('U'));
        return DB::query("SELECT id, title FROM evaluations WHERE target_group = '$userType' AND period_start <= '$now' AND '$now' <= period_end AND '$userId' NOT IN (SELECT DISTINCT user_id FROM evaluation_participants WHERE evaluation_id = id)", $res, 'DB::CB_GET');
    });
}