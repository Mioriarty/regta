<?php

function registerSchoolYearRoutes($app) {

    $app->get('', function($req, $res, $args) {
        $jsonRes = array('weeks' => [], 'vacations' => []);
        
        $q = DB::queryRaw("SELECT * FROM weeks");
        while($week = $q->fetch_assoc())
            $jsonRes['weeks'][] = $week;
        
        $q = DB::queryRaw("SELECT * FROM vacations");
        while($vac = $q->fetch_assoc())
            $jsonRes['vacations'][] = $vac;
        
        return DB::query("SELECT * FROM schoolyear", $res, function($q) use ($jsonRes) {
            while($row = $q->fetch_assoc())
                $jsonRes[$row['name']] = $row['date'];
            
            return [ $jsonRes, 200 ];
        });
    });

    $app->put('', function($req, $res, $args) {
        DB::queryRaw('DELETE FROM schoolyear');
        DB::queryRaw('DELETE FROM weeks');
        DB::queryRaw('DELETE FROM vacations');

        $body = $req->getParsedBody();

        $yearStart = DB::escape($body['year_start']);
        $yearEnd = DB::escape($body['year_end']);
        $courseSelectionStart = DB::escape($body['course_selection_start']);
        $courseSelectionEnd = DB::escape($body['course_selection_end']);
        DB::queryRaw("INSERT INTO schoolyear VALUES ('year_start', '$yearStart'), ('year_end', '$yearEnd'), ('course_selection_start', '$courseSelectionStart'), ('course_selection_end', '$courseSelectionEnd')");

        $weeks = $body['weeks'];
        $values = [];
        foreach ($weeks as $w) {
            $w = DB::escapeArray($w);
            $values[] = "('{$w['week_nr']}', '{$w['year']}', '{$w['ab']}')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO weeks VALUES $values");

        $vacations = $body['vacations'];
        $values = [];
        foreach ($vacations as $v) {
            $v = DB::escapeArray($v);
            $values[] = "('{$v['title']}', '{$v['start']}', '{$v['end']}')";
        }
        $values = implode(', ', $values);
        DB::queryRaw("INSERT INTO vacations VALUES $values");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    });


    $app->get('/vacations-external', function ($req, $res, $args) {
        $yearStart = strtotime($req->getQueryParams()['yearStart']);
        $yearEnd   = strtotime($req->getQueryParams()['yearEnd']) + 23 * 60 * 60; // add almost a day to get all neighboring vacations
        $allVacations = [];

        // parse vacations
        $vacations = json_decode(file_get_contents("https://ferien-api.de/api/v1/holidays/SN"));
        foreach($vacations as $v) {
            $vStart = strtotime($v->start);
            $vEnd   = strtotime($v->end);
            
            if($vEnd > $yearStart && $vStart < $yearEnd) {
                $allVacations[] = array(
                    'title' => ucfirst($v->name),
                    'start' => date("Y-m-d", $vStart),
                    'end'   => date("Y-m-d", $vEnd)
                );
            }
        }

        // parse single hollidays
        $vacations = json_decode(file_get_contents("https://get.api-feiertage.de/?states=sn"))->feiertage;
        foreach($vacations as $v) {
            $date = strtotime($v->date);

            if($yearStart <= $date && $yearEnd >= $date) {
                $allVacations[] = array(
                    'title' => $v->fname,
                    'start' => date("Y-m-d", $date),
                    'end' => date("Y-m-d", $date)
                );
            }
        }

        $res->getBody()->write(json_encode($allVacations));
        return $res;
    });

}

