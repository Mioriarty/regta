<?php

require("db.php");
require("authenticate.php");

function registerRoutes($app) {

    $app->post('/login', 'loginEndpoint');

    // ************************
    // ********* GET **********
    // ************************

    $app->get('/teachers', function($req, $res, $args) {
        return DB::query('SELECT id, class, title, name FROM teachers ORDER BY class', $res, 'DB::CB_GET');
    });

    $app->get('/students', function($req, $res, $args) {
        $order = isset($req->getQueryParams()['orderByClass']) ? 'class, name, prename' : 'name, prename, class';
        
        $whereClause = '';
        if($req->getAttribute('userType') === 'teacher') {
            // only show students of the class
            $teacherId = DB::escape($req->getAttribute('userId'));
            $class = DB::queryRaw("SELECT class FROM teachers WHERE id = '$teacherId'")->fetch_row()[0];
            $whereClause = "WHERE class = '$class'";
        }

        // only show himself if request comes form student
        if($req->getAttribute('userType') === 'student') {
            // only show students of the class
            $studentId = DB::escape($req->getAttribute('userId'));
            $whereClause = "WHERE id = '$studentId'";
        }

        // dont show password if request comes from trainer
        $colomns = "*";
        if($req->getAttribute('userType') === 'trainer') {
            $colomns = "id, prename, name, `class`";
        }

        return DB::query("SELECT $colomns, (SELECT GROUP_CONCAT(DISTINCT course_id) FROM students_in_courses WHERE student_id = students.id) as courses FROM students $whereClause ORDER BY $order", $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other', 'teacher', 'trainer', 'student']));

    $app->get('/courses', function($req, $res, $args) {
        $order = isset($req->getQueryParams()['orderBySector']) ? 'sector, title' : 'title';
        return DB::query("SELECT courses.id as id, courses.title as title, trainer_id, sector, place, class_levels, size, description, schedule, trainers.title as trainer_title, trainers.name as trainer_name, (SELECT COUNT(student_id) FROM students_in_courses WHERE course_id = courses.id) as count_students FROM courses INNER JOIN trainers ON trainer_id = trainers.id ORDER BY $order", $res, 'DB::CB_GET');
    });

    $app->get('/others', function($req, $res, $args) {
        return DB::query('SELECT id, name FROM others', $res, 'DB::CB_GET');
    })->add(genAuthMiddleware('other'));

    $app->get('/trainers', function($req, $res, $args) {
        $whereClause = '';
        if($req->getAttribute('userType') === 'trainer')
            $whereClause = "WHERE id = '" . $req->getAttribute('userId') . "'"; // trainers can only see their own data
        return DB::query('SELECT id, title, mail, name, (SELECT GROUP_CONCAT(id ORDER BY title) FROM courses WHERE trainer_id = trainers.id) as courses FROM trainers ' . $whereClause, $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other', 'trainer']));

    $app->get('/class/{grade}/{num}/students', function($req, $res, $args) {
        $class = DB::escape("{$args['grade']}/{$args['num']}");
        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT class FROM teachers WHERE id = '$teacherId'")->fetch_row()[0] != $class)
                throw new HttpNotAuthorizedException();
        }

        return DB::query("SELECT *, (SELECT GROUP_CONCAT(DISTINCT course_id) FROM students_in_courses WHERE student_id = students.id) as courses FROM students WHERE class = '$class' ORDER BY name, prename", $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->get('/student/{student-id}/courses', function($req, $res, $args) {
        $sId = DB::escape($args['student-id']);
        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT students.id FROM students INNER JOIN teachers ON students.class = teachers.class WHERE students.id = '$sId' AND teachers.id = '$teacherId'")->num_rows === 0) {
                // Student is not in the class of the teacher
                throw new HttpNotAuthorizedException();
            }
        }
        return DB::query("SELECT courses.id as id, visit_voluntary, courses.title as title, trainer_id, sector, place, class_levels, size, description, schedule, trainers.title as trainer_title, trainers.name as trainer_name FROM students_in_courses INNER JOIN courses ON course_id = courses.id INNER JOIN trainers ON trainer_id = trainers.id WHERE student_id = '$sId'", $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->get('/trainers/{trainer-id}/courses', function($req, $res, $args) {
        $tId = DB::escape($args['trainer-id']);
        return DB::query("SELECT * FROM courses WHERE trainer_id = '$tId'", $res, 'DB::CB_GET');
    })->add(genAuthMiddleware(['other', 'trainer']));



    // ************************
    // ********* POST *********
    // ************************

    $app->post('/students', function ($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        if($req->getAttribute('userType') === 'teacher')
            $body['class'] = DB::queryRaw("SELECT class FROM teachers WHERE id = '" . $req->getAttribute('userId') . "'")->fetch_array()[0];
        
        return DB::query("INSERT INTO students (prename, name, pwd, class) VALUES ('{$body['prename']}', '{$body['name']}', '{$body['pwd']}', '{$body['class']}')", $res, 'DB::CB_POST');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->post('/teachers', function ($req, $res, $args) {
        $pwd = password_hash($req->getParsedBody()['pwd'], PASSWORD_DEFAULT);
        $body = DB::escapeArray($req->getParsedBody());
        $q = DB::queryRaw("SELECT id FROM teachers WHERE class = '{$body['class']}'");
        if($q->num_rows > 0) {
            $res->getBody()->write(json_encode(array("error" => "Die Klasse {$body['class']} existiert schon")));
            return $res->withStatus(409);
        }
        return DB::query("INSERT INTO teachers (class, title, name, pwd) VALUES ('{$body['class']}', '{$body['title']}', '{$body['name']}', '$pwd')", $res, 'DB::CB_POST');
    })->add(genAuthMiddleware('other'));

    $app->post('/courses', function ($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        if($req->getAttribute('userType') === 'trainer')
            $body['trainerId'] = $req->getAttribute('userId');

        return DB::query("INSERT INTO courses (title, trainer_id, sector, place, class_levels, size, description, schedule) VALUES ('{$body['title']}', '{$body['trainerId']}', '{$body['sector']}', '{$body['place']}', '{$body['classLevels']}', '{$body['size']}', '{$body['description']}', '{$body['schedule']}')", $res, 'DB::CB_POST');
    })->add(genAuthMiddleware(['other', 'trainer']));

    $app->post('/trainers', function ($req, $res, $args) {
        $pwd = password_hash($req->getParsedBody()['pwd'], PASSWORD_DEFAULT);
        $body = DB::escapeArray($req->getParsedBody());
        return DB::query("INSERT INTO trainers (title, name, mail, pwd) VALUES ('{$body['title']}', '{$body['name']}', '{$body['mail']}', '$pwd')", $res, 'DB::CB_POST');
    })->add(genAuthMiddleware('other'));

    $app->post('/others', function ($req, $res, $args) {
        $pwd = password_hash($req->getParsedBody()['pwd'], PASSWORD_DEFAULT);
        $body = DB::escapeArray($req->getParsedBody());
        return DB::query("INSERT INTO others (name, pwd) VALUES ('{$body['name']}', '$pwd')", $res, 'DB::CB_POST');
    })->add(genAuthMiddleware('other'));



    // ************************
    // ********* PUT **********
    // ************************

    function genUpdateQueryFromValues($possibleVals, $body, $hashPwd = true) {
        $stm = [];

        foreach($possibleVals as $bodyKey => $dbCol) {
            if(isset($body[$bodyKey])) {
                // Hash pwd if necessery
                if($hashPwd && $bodyKey === 'pwd') {
                    $val = password_hash($body[$bodyKey], PASSWORD_DEFAULT);
                } else {
                    $val = DB::escape($body[$bodyKey]);
                }

                $stm[] = "`$dbCol` = '{$val}'";
            }
        }

        return implode(', ', $stm);
    }

    $app->put('/students/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);
        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT students.id FROM students INNER JOIN teachers ON students.class = teachers.class WHERE students.id = '$id' AND teachers.id = '$teacherId'")->num_rows === 0) {
                // Student is not in the class of the teacher
                throw new HttpNotAuthorizedException();
            }
            // teachers can not change the class
            $setStm = genUpdateQueryFromValues(['prename' => 'prename', 'name' => 'name', 'pwd' => 'pwd'], $req->getParsedBody(), false);
        } else {
            // others can to everything
            $setStm = genUpdateQueryFromValues(['prename' => 'prename', 'name' => 'name', 'class' => 'class', 'pwd' => 'pwd'], $req->getParsedBody(), false);
        }
        return DB::query("UPDATE students SET $setStm WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->put('/others/{id:[0-9]+}', function($req, $res, $args) {
        $setStm = genUpdateQueryFromValues(['name' => 'name', 'pwd' => 'pwd'], $req->getParsedBody());
        $id     = DB::escape($args['id']);
        return DB::query("UPDATE others SET $setStm WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware('other'));

    $app->put('/teachers/{id:[0-9]+}', function($req, $res, $args) {
        $setStm = genUpdateQueryFromValues(['name' => 'name', 'title' => 'title', 'class' => 'class', 'pwd' => 'pwd'], $req->getParsedBody());
        $id     = DB::escape($args['id']);
        $class  = DB::escape($req->getParsedBody()['class']);

        if($req->getAttribute('userType') == 'teacher' && $req->getAttribute('userId') != $id)
            throw new HttpNotAuthorizedException(); // teacher tried to change data of different teacher

        $q = DB::queryRaw("SELECT id FROM teachers WHERE class = '$class' AND NOT id = '$id'");
        if($q->num_rows > 0) {
            $res->getBody()->write(json_encode(array("error" => "Die Klasse $class existiert schon", "status" => "failed")));
            return $res->withStatus(409);
        }
        return DB::query("UPDATE teachers SET $setStm WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->put('/courses/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);
        if($req->getAttribute('userType') === 'teacher') {
            $trainerId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT id FROM courses WHERE id = '$id' AND trainer_id = '$trainerId'")->num_rows === 0) {
                // Course is not led by that trainer
                throw new HttpNotAuthorizedException();
            }
            // trainer can not change the trainer_id of a course
            $setStm = genUpdateQueryFromValues(['title' => 'title', 'trainerId' => $trainerId, 'sector' => 'sector', 'place' => 'place', 'classLevels' => 'class_levels', 'size' => 'size', 'description' => 'description',  'schedule' => 'schedule'], $req->getParsedBody());
        } else {
            $setStm = genUpdateQueryFromValues(['title' => 'title', 'trainerId' => 'trainer_id', 'sector' => 'sector', 'place' => 'place', 'classLevels' => 'class_levels', 'size' => 'size', 'description' => 'description',  'schedule' => 'schedule'], $req->getParsedBody());
        }
        return DB::query("UPDATE courses SET $setStm WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other', 'trainer']));

    $app->put('/trainers/{id:[0-9]+}', function($req, $res, $args) {
        $setStm = genUpdateQueryFromValues(['title' => 'title', 'name' => 'name', 'place' => 'place', 'mail' => 'mail', 'pwd' => 'pwd'], $req->getParsedBody());
        $id     = DB::escape($args['id']);

        if($req->getAttribute('userType') == 'trainer' && $req->getAttribute('userId') != $id)
            throw new HttpNotAuthorizedException(); // teacher tried to change data of different teacher

        return DB::query("UPDATE trainers SET $setStm WHERE id = '$id'", $res, 'DB::CB_PUT');
    })->add(genAuthMiddleware(['other', 'trainer']));

    $app->put('/students/{id:[0-9]+}/courses', function($req, $res, $args) {
        $entries = $req->getParsedBody()['entries'];
        $id      = DB::escape($args['id']);
        DB::queryRaw("DELETE FROM students_in_courses WHERE student_id = '$id'");

        if(count($entries) === 0) {
            $res->getBody()->write(json_encode(array("status" => "ok")));
            return $res;
        }

        $values = [];
        foreach ($entries as $e) {
            $values[] = "('$id', '{$e['courseId']}', '{$e['visit_voluntary']}')";
        }
        $values = implode(', ', $values);

        return DB::query("INSERT INTO students_in_courses (student_id, course_id, visit_voluntary) VALUES $values", $res, 'DB::CB_ALWAYS_OK');
    })->add(genAuthMiddleware('other'));

    $app->put('/courses/{id:[0-9]+}/participants', function($req, $res, $args) {
        $id = DB::escape($args['id']);
        if($req->getAttribute('userType') === 'teacher') {
            $trainerId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT id FROM courses WHERE id = '$id' AND trainer_id = '$trainerId'")->num_rows === 0) {
                // Course is not led by that trainer
                throw new HttpNotAuthorizedException();
            }
        }

        $toBeAdded = $req->getParsedBody()['students'];
        $toBeRemoved = [];

        $q = DB::queryRaw("SELECT student_id FROM students_in_courses WHERE course_id = '$id'");
        while($row = $q->fetch_array()) {
            $index = array_search($row[0], $toBeAdded);
            if($index === false) {
                $toBeRemoved[] = $row[0];
            } else {
                // Is allready in DB
                unset($toBeAdded[$index]);
            }
        }

        $toBeAdded = DB::escapeArray($toBeAdded);

        $removeCond = implode(" OR ", array_map(function ($i) { return "student_id = '$i'"; }, $toBeRemoved));
        $addStm = implode(", ", array_map(function ($i) use ($id) { return "('$i', '$id', '[]')"; }, $toBeAdded));

        DB::queryRaw("INSERT INTO students_in_courses (student_id, course_id, visit_voluntary) VALUES $addStm");
        DB::queryRaw("DELETE FROM students_in_courses WHERE course_id = '$id' AND ($removeCond)");

        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    })->add(genAuthMiddleware(['other', 'trainer']));


    // ************************
    // ******** DELETE ********
    // ************************

    // ***** CHECK DELETE *****

    function genCheckDeleteResponse($res, $canDelete, $warning = '') {
        $res->getBody()->write(json_encode(array("result" => ($canDelete ? 'passed' : 'failed'), "warning" => $warning)));
        return $res;
    }

    function checkDelete($userType, $userId) {
        switch($userType) {
            case 'student':
                return [ true, '' ];
            
            case 'course':
                $q = DB::queryRaw("SELECT student_id FROM students_in_courses WHERE course_id = '$userId'");
        
                if($q->num_rows === 0) {
                    return [ true, '' ];
                } else {
                    return [ true, 'Schüler sind in dem Kurs eingeschrieben! Es würden alle Schüler ausgetragen.'];
                }
            
            case 'trainer':
                $q = DB::queryRaw("SELECT id FROM courses WHERE trainer_id = '$userId'");

                if($q->num_rows === 0) {
                    return [ true, '' ];
                } else {
                    return [ false, 'Es werden noch Kurse von dem Kursleiter geleitet!'];
                }
            
            case 'teacher':
                $q = DB::queryRaw("SELECT students.id FROM students INNER JOIN teachers ON students.class = teachers.class WHERE teachers.id = '$userId'");
        
                if($q->num_rows === 0) {
                    return [ true, '' ];
                } else {
                    return [ false, 'Schüler befinden sich in der Klasse!'];
                }
            
            case 'other':
                $q = DB::queryRaw("SELECT id FROM others");

                if($q->num_rows <= 1) {
                    return [ false , 'Sie sind der letzte Admin-Benutzer!' ];
                } else {
                    return [ true, ''];
                }
        }
    }

    $app->get('/{userType:other|trainer|teacher}s/{id}/check-delete', function($req, $res, $args) {
        [ $canDelete, $warning ] = checkDelete(DB::escape($args['userType']), DB::escape($args['id']));
        return genCheckDeleteResponse($res, $canDelete, $warning);
    })->add(genAuthMiddleware('other'));

    $app->get('/courses/{id}/check-delete', function($req, $res, $args) {
        [ $canDelete, $warning ] = checkDelete('course', DB::escape($args['id']));
        return genCheckDeleteResponse($res, $canDelete, $warning);
    })->add(genAuthMiddleware(['other', 'trainer']));

    $app->get('/students/{id}/check-delete', function($req, $res, $args) {
        [ $canDelete, $warning ] = checkDelete('student', DB::escape($args['id']));
        return genCheckDeleteResponse($res, $canDelete, $warning);
    })->add(genAuthMiddleware(['other', 'teacher']));



    // **** ACTUAL DELETES ***

    $app->delete('/students/{id}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        if(!checkDelete('student', $id)[0])
            throw new HttpNotAuthorizedException();
        
        if($req->getAttribute('userType') === 'teacher') {
            $teacherId = $req->getAttribute('userId');
            if(DB::queryRaw("SELECT students.id FROM students INNER JOIN teachers ON students.class = teachers.class WHERE students.id = '$id' AND teachers.id = '$teacherId'")->num_rows === 0) {
                // Student is not in the class of the teacher
                throw new HttpNotAuthorizedException();
            }
        }
        
        return DB::query("DELETE FROM students WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware(['other', 'teacher']));

    $app->delete('/courses/{id}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        if(!checkDelete('course', $id)[0])
            throw new HttpNotAuthorizedException();

        if($req->getAttribute('userType') == 'trainer') {
            if(DB::queryRaw("SELECT trainer_id FROM courses WHERE id = $id")->fetch_assoc()['trainer_id'] != $req->getAttribute('userId')) {
                // trainer wants to delete a course he doesnt lead
                throw new HttpNotAuthorizedException();
            }
        }

        DB::queryRaw("DELETE FROM courses WHERE id = '$id'");
        DB::queryRaw("DELETE FROM students_in_courses WHERE course_id = '$id'");
        return DB::query("DELETE FROM attendances WHERE course_id = '$id'", $res, 'DB::CB_ALWAYS_OK');
    })->add(genAuthMiddleware(['other', 'trainer']));


    $app->delete('/trainers/{id}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        if(!checkDelete('trainer', $id)[0])
            throw new HttpNotAuthorizedException();
            
        return DB::query("DELETE FROM trainers WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware('other'));

    $app->delete('/teacher/{id}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        if(!checkDelete('teacher', $id)[0])
            throw new HttpNotAuthorizedException();
        
        return DB::query("DELETE FROM teachers WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware('other'));

    $app->delete('/others/{id}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        if(!checkDelete('other', $id)[0])
            throw new HttpNotAuthorizedException();
        
        return DB::query("DELETE FROM others WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware('other'));


    // ************************
    // ***** OTHER TOPICS *****
    // ************************

    require("schoolYear.php");
    $app->group('/school-year', 'registerSchoolYearRoutes')->add(genAuthMiddleware('other'));

    require("attendance.php");
    $app->group('/attendances', 'registerAttendanceRoutes');

    require("pdfGenerators/pdfRoutes.php");
    $app->group('/pdfs', 'registerPdfRoutes')->add(genAuthMiddleware('other'));

    require("termParticipants.php");
    $app->group('/termParticpants', 'registerTermParticipantsRoutes')->add(genAuthMiddleware(['other', 'trainer']));

    require("evaluation.php");
    $app->group('/evaluations', 'registerEvaluationManagementRoutes')->add(genAuthMiddleware('other'));
    $app->group('/evaluations/questionnaire', 'registerEvaluationQuestionnaireRoutes')->add(genAuthMiddleware(['other', 'trainer', 'student']));

    require("studentForwarding.php");
    $app->group('/studentForwarding', 'registerStudentForwardingRoutes')->add(genAuthMiddleware(['other', 'teacher']));

    require("courseSelection.php");
    $app->group('/courseSelection', 'registerCourseSelectionRoutes')->add(genAuthMiddleware(['other', 'student']));

    require("backups.php");
    $app->group('/backups', 'registerBackupsRoutes')->add(genAuthMiddleware('other'));

    require("feedback.php");
    $app->group('/feedback', 'registerFeedbackRoutes')->add(genAuthMiddleware(['other', 'trainer', 'teacher', 'student']));

    // ************************
    // **** TESTING SPACE *****
    // ************************

    // $app->get('/testing', function($req, $res, $args) {
    //     var_dump(PHP_INT_MAX);
    //     die();
    // });

    
}