<?php

function writePage($pdf, $text, $variables) {
    foreach ($variables as $name => $value) {
        $text = str_replace('$$' . $name . '$$', $value, $text);
    }
    $pdf->addPage();
    $pdf->writeHtml((new Parsedown())->parse($text));
}

function registerCustomTemplateRoutes($app) {

    $app->get('', function($req, $res, $args) {
        return DB::query('SELECT * FROM pdf_templates', $res, 'DB::CB_GET');
    });

    $app->post('', function ($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());

        return DB::query("INSERT INTO pdf_templates (title, target_group, `text`, variables) VALUES ('{$body['title']}', '{$body['targetGroup']}', '{$body['text']}', '{$body['variables']}')", $res, 'DB::CB_POST');
    });

    $app->put('/{id:[0-9]+}', function($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        $id   = DB::escape($args['id']);

        return DB::query("UPDATE pdf_templates SET title = '{$body['title']}', target_group = '{$body['targetGroup']}', `text` = '{$body['text']}', variables = '{$body['variables']}' WHERE id = '$id'", $res, 'DB::CB_PUT');
    });

    $app->delete('/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);
        
        return DB::query("DELETE FROM pdf_templates WHERE id = '$id'", $res, 'DB::CB_DELETE');
    })->add(genAuthMiddleware('other'));

    $app->get('/generate/{id:[0-9]+}', function($req, $res, $args) {
        $id = DB::escape($args['id']);

        $template = DB::queryRaw("SELECT * FROM pdf_templates WHERE id = '$id'")->fetch_assoc();

        $pdf = new GTAPDF($template['title']);

        $variables = DB::escapeArray($req->getQueryParams());

        if($template['target_group'] == 'none') {
            writePage($pdf, $template['text'], $variables);
        } elseif($template['target_group'] == 'student') {
            $students = DB::queryRaw("SELECT name, prename, class, pwd FROM students ORDER BY class, name, prename");
            while($student = $students->fetch_assoc()) {
                $variables['VORNAME'] = $student['prename'];
                $variables['NAME'] = $student['name'];
                $variables['INITIALIEN'] = strtoupper($student['prename'][0] . $student['name'][0]);
                $variables['KLASSE'] = $student['class'];
                $variables['PASSWORT'] = $student['pwd'];

                writePage($pdf, $template['text'], $variables);
            }
        } elseif($template['target_group'] == 'student_in_course') {
            $students = DB::queryRaw("SELECT name, prename, class, pwd, GROUP_CONCAT(courses.title SEPARATOR ', ') AS courses FROM courses INNER JOIN students_in_courses ON courses.id = course_id INNER JOIN students ON student_id = students.id GROUP BY student_id ORDER BY class, name, prename");
            while($student = $students->fetch_assoc()) {
                $variables['VORNAME'] = $student['prename'];
                $variables['NAME'] = $student['name'];
                $variables['INITIALIEN'] = strtoupper($student['prename'][0] . $student['name'][0]);
                $variables['KLASSE'] = $student['class'];
                $variables['KURSE'] = $student['courses'];
                $variables['PASSWORT'] = $student['pwd'];

                writePage($pdf, $template['text'], $variables);
            }
        } elseif($template['target_group'] == 'trainer') {
            $trainers = DB::queryRaw("SELECT CONCAT(trainers.title, ' ', name) AS name, GROUP_CONCAT(courses.title SEPARATOR ', ') AS courses FROM trainers INNER JOIN courses ON trainers.id = trainer_id GROUP BY trainers.id");
            while($trainer = $trainers->fetch_assoc()) {
                $variables['NAME'] = $trainer['name'];
                $variables['KURSE'] = $trainer['courses'];

                writePage($pdf, $template['text'], $variables);
            }
        } else {
            $res = $res->withStatus(400);
            $res->getBody()->write(json_encode(array("error" => "invalid target group submitted", "status" => "failed")));
        }


        return $pdf->writeToResponse($res);

    });
}