<?php

class GTAPDF extends TCPDF {

    private $fontname;

    public function __construct($title) {
        parent::__construct('P', 'mm', 'A4', true, 'UTF-8', false);

        $this->fontname = TCPDF_FONTS::addTTFfont('res/open_sans_regular.ttf', 'TrueTypeUnicode', '', 96);
        TCPDF_FONTS::addTTFfont('res/open_sans_bold.ttf', 'TrueTypeUnicode', '', 96);
        TCPDF_FONTS::addTTFfont('res/open_sans_italic.ttf', 'TrueTypeUnicode', '', 96);
        TCPDF_FONTS::addTTFfont('res/open_sans_bolditalic.ttf', 'TrueTypeUnicode', '', 96);

        $this->SetAuthor('Gerda-Taro-Schule Leipzig');
        $this->SetTitle($title);
        $this->SetSubject($title);

        $this->SetMargins(PDF_MARGIN_LEFT, 80, PDF_MARGIN_RIGHT); // the middle value is the header height
        $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        $this->setPrintFooter(false);
    }

    public function Header() {
        $this->Ln(9);
        $this->SetFont($this->fontname);
        $this->SetFontSize(12);
        $this->Cell(170, 5, "GERDA-TARO-SCHULE", 0, 1);
        $this->Cell(170, 5, "GYMNASIUM DER STADT LEIPZIG", 0, 1);
        $this->Cell(170, 5, "Telemannstraße 9", 0, 1);
        $this->Cell(170, 5, "04107 Leipzig", 0, 1);
        $this->SetFontSize(10);
        $this->Ln();
        $this->Cell(170, 4, "Tel.: 0341 14909800", 0, 1);
        $this->Cell(170, 4, "Fax: 0341 149098025", 0, 1);
        $this->Cell(170, 4, "sekretariat@gts.lernsax.de", 0, 1);
        $this->Cell(170, 4, "https://cms.sachsen.schule/gymltele", 0, 1);

        
        $this->ImageSVG($file='res/LogoTaroGymnasium.svg', $x=105, $y=9, $w=80, $h=35, $link='', $align='', $palign='', $border=0, $fitonpage=false);
        $this->setY(45);
        $this->setX(105);
        $this->SetFontSize(8);
        $this->Cell(170, 4, "Schule mit Schwerpunkt Medien- und Informatikbildung M. I. T.");
    }

    public function addPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false) {
        parent::addPage($orientation, $format, $keepmargins, $tocpage);
        $this->setFont($this->fontname, '', 10);
    }


    public function writeToResponse($res) {
        $res = $res->withHeader('Content-type', 'application/pdf')
                   ->withAddedHeader('Content-Disposition', 'inline; filename=' . $this->title . '.pdf');
        $res->getBody()->write($this->Output($this->title . '.pdf', 'S'));
        return $res;
    }

}