<?php

require("trainerAttendancesPdf.php");
require("evauationAnalysisPdf.php");
require("customPdfTemplates.php");
require("GTAPDF.php");


function registerPdfRoutes($app) {

    $app->get('/trainerAttendances', 'trainerAttendancesPdf');
    $app->get('/analyzeEvaluation', 'evaluationAnalysisPdf');

    $app->group('/custom', 'registerCustomTemplateRoutes');

}