<?php

function printTermAtts($term, $attsOfTerm, $countDates, &$html, $sumBold = false) {
    $present = array_filter($attsOfTerm, function($att)  {return $att['status'] == 1; });
    $excused = array_filter($attsOfTerm, function($att)  {return $att['status'] == 2; });
    $notExcused = array_filter($attsOfTerm, function($att)  {return $att['status'] == 3; });
    $noInfoCount = $countDates - count($present) - count($excused) - count($notExcused);

    $durationInHours = round((strtotime($term->endTime) - strtotime($term->startTime)) / 60 / 60, 2);

    if($countDates == 1) {
        $html .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        if(count($present) > 0)
            $html .= "Anwesend";
        elseif(count($excused) > 0)
            $html .= "Entschuldigt";
        elseif(count($notExcused) > 0)
            $html .= "Unentschudligt";
        else
            $html .= "Keine Informationen";
    } else {

        $html .= "<ul><li>Anwesend: " . count($present) . " Mal</li>";
        $html .= "<li>Entschuldigt: " . count($excused) . " Mal";
        if(count($excused) > 0)
            $html .= " (" . implode(', ', array_map(function($att) {return date("d.m.y", strtotime($att['date']));}, $excused)) . ")";
        
        $html .= "</li><li>Unentschuldigt: " . count($notExcused) . " Mal";
        if(count($notExcused) > 0)
            $html .= " (" . implode(', ', array_map(function($att) {return date("d.m.y", strtotime($att['date']));}, $notExcused)) . ")";

        if($noInfoCount)
            $html .= "</li><li>Keine Information: $noInfoCount Termine";

        $html .= "</li></ul>";

    }
    $html .= '<div style="text-align: right;">' . ($sumBold ? '<h3>' : '<b>') . count($present) ." x {$durationInHours}h = " . count($present) * $durationInHours .'h' . ($sumBold ? '</h3>' : '</b>') .'</div>';
    
    return count($present) * $durationInHours;
    
}

function trainerAttendancesPdf($req, $res, $args) {
    $q = DB::queryRaw("SELECT date FROM schoolyear WHERE name = 'year_start' OR name = 'year_end' ORDER BY name DESC");
    $pdf = new GTAPDF('Anwesenheiten (' . substr($q->fetch_row()[0], 2, 2) . ' ' . substr($q->fetch_row()[0], 2, 2) . ')');

    $courses = DB::queryRaw("SELECT *, courses.id as id, courses.title as title, trainers.title as trainer_title, trainers.name as trainer_name FROM courses INNER JOIN trainers ON trainers.id = trainer_id ORDER BY trainer_id");
    
    while($c = $courses->fetch_assoc()) {
        $atts = DB::queryRaw("SELECT * FROM attendances WHERE course_id = '{$c['id']}' AND student_id = '-1'")->fetch_all(MYSQLI_ASSOC);
        $terms = json_decode($c['schedule']);
        $dates = generateAllDates($terms, '+3 years'); // to get really all dates of the school year

        $pdf->addPage();
        $html = "<h2>Anwesenheit von {$c['trainer_title']} {$c['trainer_name']} vom Kurs {$c['title']}</h2><br />";

        
        
        if(count($terms) == 0) {
            continue;
        } elseif(count($terms) == 1) {
            // just to make sure
            $attsOfTerm = array_filter($atts, function($att) use ($terms) { return $terms[0]->uid == $att['term_id'];});

            $html .= "Der Kurs sollte in diesem Schuljahr " . count($dates) . " Mal stattfinden.";
            printTermAtts($terms[0], $attsOfTerm,  count($dates), $html, true);
        } else {
            $html .= "<p>Der Kurse hatte verschiedenartige Termin, an dem er stattfand:</p><ul>";
            $sumOfEffort = 0;

            foreach($terms as $term) {
                $datesOfTerm = array_filter($dates, function($d) use ($term) { return $term->uid == $d['term_id']; });
                $attsOfTerm = array_filter($atts, function($att) use ($term) { return $term->uid == $att['term_id']; });

                $html .= "<li><b>{$term->title}</b> sollte " . count($datesOfTerm) . " Mal stattfinden.";
                $sumOfEffort += printTermAtts($term, $attsOfTerm,  count($datesOfTerm), $html);
                $html .= "</li>";
            }

            $html .= "</ul>";

            $html .= '<h3 style="text-align: right;">Gesamt: ' . $sumOfEffort .'h</h3>';
        }

        $pdf->writeHTML($html, true, 0, true, 0);
        
            
    }
    return $pdf->writeToResponse($res);
}