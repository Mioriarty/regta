<?php

define('TABLE_DARK', '#984806');
define('TABLE_LIGHT', '#FABF8F');
define('TABLE_VERY_LIGHT', '#FEF4EC');

define('CHART_DARK', '#5E221D');


function chart($width, $height, $data) {
    return '<div align="center"><img src="https://quickchart.io/chart?width=' . $width . '&height=' . $height . '&c=' . urlencode($data) . '" width="' . $width .'"/></div>';
}

function countAnswerApearances($answers, $possibleAnswers) {
    
    $result = array_fill_keys($possibleAnswers, 0);
    foreach($answers as $answer) {
        if(array_key_exists($answer, $result)) {
            $result[$answer] += 1;
        } else {
            $result[$answer] = 0;
        }
    }
    return $result;
}

function participantCount($evalId) {
    $html = "<h3>Teilnahmen an der Umfrage</h3>";
    $q = DB::queryRaw("SELECT class, (SELECT COUNT(*) FROM evaluation_participants INNER JOIN students ON user_id = students.id WHERE evaluation_id = '$evalId' AND students.class = teachers.class) AS eval_participants, (SELECT COUNT(DISTINCT student_id) FROM students_in_courses INNER JOIN students ON student_id = students.id WHERE students.class = teachers.class) AS gta_participants, (SELECT COUNT(*) FROM students WHERE students.class = teachers.class) AS classmates FROM teachers ORDER BY class");
    $classes = [];
    $evalParticipantsByClass = [];
    $gtaParticipantsByClass = [];
    $totalEvalParticipants = 0;
    $totalGtaParticipants = 0;
    $totalStudents = 0;

    while($r = $q->fetch_assoc()) {
        $classes[] = $r['class'];
        $evalParticipantsByClass[] = $r['eval_participants'];
        $gtaParticipantsByClass[] = $r['gta_participants'];

        $totalEvalParticipants += $r['eval_participants'];
        $totalGtaParticipants += $r['gta_participants'];
        $totalStudents += $r['classmates'];
    }
    $evalPercentage = number_format($totalEvalParticipants / $totalStudents * 100, 1, ',', '');
    $gtaPercentage = number_format($totalGtaParticipants / $totalStudents * 100, 1, ',', '');
    $html .= "<p>Insgesamt sind $totalStudents Schüler*innen an der Schule. Davon haben $totalEvalParticipants ($evalPercentage%) an dieser Umfrage teilgenommen und $totalGtaParticipants ($gtaPercentage%) nahmen mindestens ein GTA-Angebot wahr.</p><p>Hier sind die Teilnehmendenzahlen nochmal pro Klasse aufgeschlüsselt:</p>";
    
    $labels = json_encode($classes);
    $evalValues = json_encode($evalParticipantsByClass);
    $gtaValues = json_encode($gtaParticipantsByClass);
    $chartData = <<<EOD
    {
        type:  'bar', 
        data: {
            labels: $labels, 
            datasets:  [ 
                {
                    label: 'Evaluationsteilnehmende', 
                    data: $evalValues
                }, 
                {
                    label: 'GTA-Teilnehmende', 
                    data: $gtaValues
                } 
            ]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'center', 
                    color: '#FFF'
                }
            }
        }
    }
EOD;
    
    return $html . chart(600, 300, $chartData);
}

function sectionAnalysis($sectionData, $answers, $courses, $forTrainers) {
    $html = "<h3>Abschnitt: {$sectionData->title}</h3>";
    $Parsedown = new ParseDown();

    foreach($sectionData->questions as $i => $question) {
        if($question->type != 'text') {
            $parsedText = str_replace('$$KURS$$', '*Kursname*', $question->text);
            $parsedText = str_replace('$$LEITER$$', '*Kursleiter*', $parsedText);
            $html .= "<big><b><u>Frage:</u></b></big> " . $Parsedown->line($parsedText) . '<br />';
        }
        switch($question->type) {
            case 'mark': $html .= markAnalysis($question, $answers[$i], $courses, $forTrainers); break;
            case 'freeAnswer': $html .= freeAnswerAnalysis($question, $answers[$i], $courses); break;
            case 'multipleChoice': $html .= multipleChoiceAnalysis($question, $answers[$i], $courses, $forTrainers); break;
        }

        $html .= '<br /><br />';
        
    }

    return $html;
}

function makeCourseAnswerCountTableContent($courseTitle, $leftCellText, $possibleAnswers, $answerAppearances, $forTrainers, $leftWidth = NULL, $cellWidth = NULL) {
    $leftStyle = 'style="background-color:' . TABLE_DARK . ';color:white;' . ($leftWidth == NULL ? '' : "width:$leftWidth px;") . '"';
    $lightStyle = 'style="background-color:' . TABLE_LIGHT . ';color:black;' . ($cellWidth == NULL ? '' : "width:$cellWidth px;") . '" align="center"';
    $veryLightStyle = 'style="background-color:' . TABLE_VERY_LIGHT . ';color:black;' . ($cellWidth == NULL ? '' : "width:$cellWidth px;") . '" align="center"';

    if($forTrainers) {
        $answer = array_keys(array_filter($answerAppearances, function($v) { return $v > 0; }))[0];
        $html = "<tr><td style=\"" . ($leftWidth == NULL ? '' : "width:$leftWidth px;") . "\"><b>GTA</b></td><td><b>$courseTitle</b></td></tr>";
        $html .= "<tr><td $leftStyle>$leftCellText</td><td $lightStyle>$answer</td></tr>";
    } else  {
        $numPossibleAnswers = count($possibleAnswers);
        $html = "<tr><td style=\"" . ($leftWidth == NULL ? '' : "width:$leftWidth px;") . "\"><b>GTA</b></td><td colspan=\"$numPossibleAnswers\"><b>$courseTitle</b></td></tr>";
        $html .= "<tr><td rowspan=\"2\" $leftStyle>$leftCellText</td><td $lightStyle>" . implode("</td><td $lightStyle>", $possibleAnswers) . '</td></tr>';
        $html .= "<tr><td $veryLightStyle>" . implode("</td><td $veryLightStyle>", $answerAppearances) . '</td></tr>';
    }

    return $html;
}

function markAnalysis($questionData, $answers, $courses, $forTrainers) {
    $possibleAnswers = range(1, $questionData->properties->max);
    $html = '';
    $totalAnwserAppearances = array_fill_keys($possibleAnswers, 0);
    $first = true;

    

    if($questionData->showMode == 'courseSpecific') {
        $leftStyle = 'style="background-color:' . TABLE_DARK . ';color:white;width:150px;"';
        $lightStyle = 'style="background-color:' . TABLE_LIGHT . ';color:black;width:40px;" align="center"';
        $veryLightStyle = 'style="background-color:' . TABLE_VERY_LIGHT . ';color:black;" align="center"';

        $html = '<h4>Im Besonderen:</h4>';
        foreach ($answers as $courseId => $courseAnswers) {
            $answerAppearances = countAnswerApearances($courseAnswers, $possibleAnswers);
            $average = number_format(array_sum($courseAnswers) / count($courseAnswers), 1, ',', '.');
            $html .= '<table nobr="true">' . makeCourseAnswerCountTableContent($courses[$courseId], $forTrainers ? 'Note' : 'Noten',  $possibleAnswers, $answerAppearances, $forTrainers, $forTrainers ? NULL : 150, $forTrainers ? NULL : 40);
            if(!$forTrainers)
                $html .= "<tr><td style=\"background-color:" . TABLE_DARK . ";color:white;\">Gesamtnote</td><td colspan=\"{$questionData->properties->max}\" style=\"background-color: " . TABLE_LIGHT . "\" align=\"center\">$average</td></tr>";
            $html .= '</table>';
            
            foreach($possibleAnswers as $pa)
                $totalAnwserAppearances[$pa] += $answerAppearances[$pa];
        }

    } else {
        $totalAnwserAppearances = countAnswerApearances($answers[-1], $possibleAnswers);
    }

    // calculate total avg
    $avgNumerator = 0;
    $avgDenometer = 0;
    foreach($totalAnwserAppearances as $mark => $markAppearances) {
        $avgNumerator += $mark * $markAppearances;
        $avgDenometer += $markAppearances;
    }
    $totalAvg = number_format($avgNumerator / $avgDenometer, 1, ',', '');

    $labels = json_encode($possibleAnswers);
    $values = json_encode(array_values($totalAnwserAppearances));
    $chartDark = CHART_DARK;
    
    $chartData = <<<EOD
    {
        type: 'bar',
        data: {
            labels: $labels, 
            datasets: [ 
                {
                    backgroundColor: '$chartDark', 
                    data: $values
                } 
            ]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'center', 
                    color: '#FFF'
                }
            }, 
            legend: {
                display: false
            }
        }
    }
EOD;

    return "Im Durchschnitt wurde die Note $totalAvg erzielt.<br />" . chart(350, 150, $chartData) . $html;
}

function makeStripedTableContent($values, $cols) {
    $html = '';
    $even = true;


    for($i = 0; $i < count($values) / $cols; $i++) {
        $bg = $even ? TABLE_LIGHT : TABLE_VERY_LIGHT;
        $even = !$even;

        $html .= "<tr style=\"background-color: $bg;\">";
        for($col = 0; $col < $cols; $col++) {
            $value = array_key_exists($cols * $i + $col, $values) ? $values[$cols * $i + $col] : '';
            $html .= "<td>$value</td>";
        }

        $html .= '</tr>';
    }
    return $html;
}

function freeAnswerAnalysis($questionData, $answers, $courses) {
    $cols = 2;

    if($questionData->showMode == 'courseSpecific') {
        $html = '';
        

        foreach ($answers as $courseId => $courseAnswers) {
            $html .= '<table><tr><td colspan="' . $cols . '"><b>GTA: ' . $courses[$courseId] . '</b></td></tr>';
            $html .= makeStripedTableContent($courseAnswers, $cols);
            $html .= '</table>';
            
        }
        return $html;
    } else {
        return '<table>' . makeStripedTableContent($answers[-1], $cols) . '</table>';
    }
}

function parseAnswersForCounting($a) {
    return str_starts_with($a, '__other__') ? 'Sonstige' : $a;
}

function multipleChoiceAnalysis($questionData, $answers, $courses, $forTrainers) {
    $otherTableCols = 2;
    $html = '';
    $courseSpecificHtml = '';
    $possibleAnswers = $questionData->properties->options;
    if($questionData->properties->allowOther)
        $possibleAnswers[] = 'Sonstige';
    

    if($questionData->showMode == 'courseSpecific') {
        $leftStyle = 'style="background-color:' . TABLE_DARK . ';color:white;"';
        $lightStyle = 'style="background-color:' . TABLE_LIGHT . ';color:black;" align="center"';
        $veryLightStyle = 'style="background-color:' . TABLE_VERY_LIGHT . ';color:black;" align="center"';
        $totalAnwserAppearances = array_fill_keys($possibleAnswers, 0);

        $courseSpecificHtml = '<h4>Im Besonderen</h4>';
        foreach ($answers as $courseId => $courseAnswers) {
            $numPossibleAnswers = count($possibleAnswers);
            $answerAppearances = countAnswerApearances(array_map('parseAnswersForCounting', $courseAnswers), $possibleAnswers);
            $otherAnswers = array_filter($courseAnswers, function($v) { return str_starts_with($v, '__other__'); });
            $otherAnswers = array_map(function($v) { return substr($v, 9); }, $otherAnswers);
            $otherAnswers = array_values($otherAnswers);

            foreach($possibleAnswers as $pa)
                $totalAnwserAppearances[$pa] += $answerAppearances[$pa];
            
            if($forTrainers && count($otherAnswers) > 0)
                $answerAppearances = array("Sonstiges: {$otherAnswers[0]}" => 1);

            $courseSpecificHtml .= "<table nobr=\"true\">" . makeCourseAnswerCountTableContent($courses[$courseId], $forTrainers ? 'Antwort' : 'Antworten', $possibleAnswers, $answerAppearances, $forTrainers);
            if(!$forTrainers && count($otherAnswers) > 0) {
                $otherTitle = $forTrainers ? 'Sonstige Antwort' : 'Sonstige Antworten';
                $courseSpecificHtml .= "<tr><td $leftStyle rowspan=\"" . count($otherAnswers) ."\">$otherTitle</td><td colspan=\"{$numPossibleAnswers}\" style=\"background-color: " . TABLE_LIGHT . "\">{$otherAnswers[0]}</td></tr></table>";
            }
            $courseSpecificHtml .= '</table>';

            
        }
    } else {
        $totalAnwserAppearances = countAnswerApearances(array_map('parseAnswersForCounting', $answers[-1]), $possibleAnswers);
    }

    $numAnswers = array_sum($totalAnwserAppearances);

    $labels = json_encode($possibleAnswers);
    $values = json_encode(array_values($totalAnwserAppearances));
    $chartData = <<<EOD
    {
        type: 'pie', 
        data: {
            labels: $labels,
            datasets: [
                {
                    data: $values,
                }
            ]
        },
        options: {
            legend: {
                display: true,
                position: 'right'
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    display: c => c.dataset.data[c.dataIndex] != 0,
                    formatter: (v, c) => (v / $numAnswers * 100).toFixed(1).toString().concat('%')
                }
            }
        }
    }
EOD;

    $html .= chart(350, 130, $chartData);

    if($questionData->showMode != 'courseSpecific') {
        $otherAnswers = array_filter($answers[-1], function($v) { return str_starts_with($v, '__other__'); });
        $otherAnswers = array_map(function($v) { return substr($v, 9); }, $otherAnswers);
        $otherAnswers = array_values($otherAnswers);
        if(count($otherAnswers) > 0) {
            $html .= '<table><tr><td colspan="' . ($otherTableCols) . '"><b>Sonstige Antworten</b></td></tr>';
            $html .= makeStripedTableContent($otherAnswers, $otherTableCols);
            $html .= '</table>';
        }
        
    }

    return $html . $courseSpecificHtml;
}

function evaluationAnalysisPdf($req, $res, $args) {
    $body = $req->getQueryParams();
    $evalId = DB::escape($body['evalId']);

    $pdf = new GTAPDF($body['title']);
    $Parsedown = new Parsedown();
    $pdf->addPage();
    

    $intro = "<h2>{$body['title']}</h2>";
    if($body['description'] != '') {
        $intro .= $Parsedown->text($body['description']) . "<br />";
    }

    $pdf->writeHtml($intro);

    if($body['showParticipantCount'] == 'true') {
        $pdf->writeHtml(participantCount($evalId));
        $pdf->addPage();
    }
    
    $eval = DB::queryRaw("SELECT target_group, `data` FROM evaluations WHERE id = $evalId")->fetch_assoc();
    $sections = json_decode($eval['data']);
    $forTrainers = $eval['target_group'] == 'trainer';

    // get all answers and sort them by section, question and course
    $q = DB::queryRaw("SELECT * FROM evaluation_answers WHERE evaluation_id = '$evalId' ORDER BY course_id");
    $answers = array();
    while($answer = $q->fetch_assoc()) {
        if(array_key_exists($answer['section_index'], $answers)) {
            if(array_key_exists($answer['question_index'], $answers[$answer['section_index']])) {
                if(array_key_exists($answer['course_id'], $answers[$answer['section_index']][$answer['question_index']])) {
                    $answers[$answer['section_index']][$answer['question_index']][$answer['course_id']][] = $answer['answer'];
                } else {
                    $answers[$answer['section_index']][$answer['question_index']][$answer['course_id']] = [ $answer['answer'] ];
                }
            } else {
                $answers[$answer['section_index']][$answer['question_index']] = array($answer['course_id'] => [ $answer['answer'] ]);
            }
        } else {
            $answers[$answer['section_index']] = array($answer['question_index'] => array($answer['course_id'] => [ $answer['answer'] ]));
        }
    }
    

    // Get all courses and sort them by id
    $q = DB::queryRaw("SELECT id, title FROM courses");
    $courses = array();
    while($course = $q->fetch_assoc()) {
        $courses[$course['id']] = $course['title'];
    }

    // Draw sections
    foreach ($sections as $i => $sectionData) {
        $pdf->writeHtml(sectionAnalysis($sectionData, $answers[$i], $courses, $forTrainers));
        if($i < count($sections) - 1)
            $pdf->addPage();
    }


    return $pdf->writeToResponse($res);
}