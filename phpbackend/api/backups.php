<?php

function registerBackupsRoutes($app) {

    $app->get('', function($req, $res, $args) {
        $backups = array_map(function($backup) {
            return array(
                "filename" => $backup['fileInfo']->getFilename(), 
                "filesize" => filesize($backup['fileInfo']->getPathname()), 
                "description" => $backup['description'],
                "automaticallyGenerated" => $backup['automatic']
            );
        }, DB::getStoredBackups());

        $res->getBody()->write(json_encode($backups));
        return $res;
    });

    $app->post('', function($req, $res, $args) {
        $body = DB::escapeArray($req->getParsedBody());
        $filename = DB::createBackup($body['description'], false);

        $res->getBody()->write(json_encode(array("status" => "ok", "filename" => $filename)));
        return $res;
    });

    $app->delete('/{filename}', function($req, $res, $args) {
        $filename = $args['filename'];
        DB::deleteBackup($filename);
            
        $res->getBody()->write(json_encode(array("status" => "ok")));
        return $res;
    })->add(genAuthMiddleware('other'));

    $app->get('/download/{filename}', function($req, $res, $args) {
        $file = DB::getBackupPath($args['filename']);

        $res = $res->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', 'text/plaintext')
            ->withHeader('Content-Disposition', 'attachment;filename="'.basename($file).'"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->withHeader('Content-Length', filesize($file));

        $res->getBody()->write(file_get_contents($file));
    
        return $res;
    });

}