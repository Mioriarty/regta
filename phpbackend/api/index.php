<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;
use Slim\Factory\AppFactory;
use Slim\Exception\HttpNotFoundException;

class HttpNotAuthorizedException extends Exception { }

require("../vendor/autoload.php");

// Load .env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Create app
$app = AppFactory::create();

// Custom jason error handler
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$customErrorHandler = function (Request $request, Throwable $exception, bool $displayErrorDetails, bool $logErrors, bool $logErrorDetails) use ($app) {
    $res = $app->getResponseFactory()->createResponse();
    if ($exception instanceof HttpNotFoundException) {
       $res = $res->withStatus(404);
       $res->getBody()->write(json_encode(array("error" => "api endpoint ({$request->getUri()->getPath()}) not found")));
    } elseif($exception instanceof HttpNotAuthorizedException) {
        $res = $res->withStatus(403);
        $res->getBody()->write(json_encode(array("error" => 'You are not allowed to perfom this request')));
    } else {
        $res = $res->withStatus(500);
        if(isset($_ENV['DEBUG'])) {
            $res->getBody()->write(json_encode(array(
                "error" => "internal error",
                "message" => $exception->getMessage(),
                "file" => $exception->getFile(),
                "line" => $exception->getLine(),
                "trace" => $exception->getTrace()
            )));
        } else {
            $res->getBody()->write(json_encode(array("error" => "internal error")));
        }
    }

    return $res;
};
$errorMiddleware->setDefaultErrorHandler($customErrorHandler);



// enable JSON parsing
$app->add(function ($req, $handler) {
    $method = $req->getMethod();
    if($method === 'GET' || $method === 'DELETE' || $method === 'OPTIONS') {
        return $handler->handle($req);
    }

    $contentType = $req->getHeaderLine('Content-Type');

    if (strstr($contentType, 'application/json')) {
        $contents = json_decode(file_get_contents('php://input'), true);

        if (json_last_error() === JSON_ERROR_NONE) {
            $req = $req->withParsedBody($contents);
            return $handler->handle($req);
        } else {
            $res = (new \Slim\Psr7\Response())->withStatus(400);
            $res->getBody()->write(json_encode(array("error" => "JSON couldn't be parsed")));
            return $res;
        }
    } elseif (strstr($contentType, 'text/csv')) {
        return $handler->handle($req);
    } else {
        $res = (new \Slim\Psr7\Response())->withStatus(415);
        $res->getBody()->write(json_encode(array("error" => "JSON data expected")));
        return $res;
    }
    
});

// Put correct headers
$app->add(function ($req, $handler) {
    $res = $handler->handle($req);
    if(!$res->hasHeader('Content-Type'))
        $res = $res->withHeader('Content-Type', 'application/json');

    // enable CORS
    if(isset($_ENV['CORS_ALLOWED_ORIGIN'])) {
        $res = $res->withHeader('Access-Control-Allow-Origin', $_ENV['CORS_ALLOWED_ORIGIN'])
                   ->withHeader('Access-Control-Allow-Headers', 'x-access-token, Content-Type, Accept, Origin, Authorization')
                   ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
        
    }
    return $res;
});


// Register all api endpoints
require("routes.php");
$app->group('/api', function (RouteCollectorProxy $apiGroup) {
    registerRoutes($apiGroup);

    // enable preflights if cors in enabled
    if(isset($_ENV['CORS_ALLOWED_ORIGIN'])) {
        $apiGroup->any('/{route:.*}', function ($req, $res, $args) {
            if($req->getMethod() === 'OPTIONS')
                return $res;
            else
                throw new HttpNotFoundException($req);
        });
    }
    
});

// Finally run
$app->run();
?>