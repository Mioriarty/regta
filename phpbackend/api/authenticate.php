<?php

use Firebase\JWT\JWT;

function generateJWT($userId, $userType) {
    $issuedAt = new DateTimeImmutable();
    $expire   = $issuedAt->modify('+' . $_ENV['JWT_EXPIRETIME'])->getTimestamp();

    $payload = array();
    $payload['iss'] = $_SERVER['SERVER_NAME'];
    $payload['iat'] = $issuedAt->getTimestamp();
    $payload['exp'] = $expire;

    $payload['userId']   = $userId;
    $payload['userType'] = $userType;

    return [ JWT::encode($payload, $_ENV['JWT_SECRET'], 'HS512'), $payload ];
}

function verifyJWT($req) {
    $res = (new \Slim\Psr7\Response())->withStatus(401);

    if(!$req->hasHeader('x-access-token')) {
        $res->getBody()->write(json_encode(array("error" => "Please privide an authentication token in the x-access-token header")));
        return $res;
    }

    try {
        $token = JWT::decode($req->getHeader('x-access-token')[0], $_ENV['JWT_SECRET'], ['HS512']);
    } catch (Exception $e) {
        if(isset($_ENV['DEBUG'])) {
            $res->getBody()->write(json_encode(array("error" => "JWT decode error: " . $e->getMessage())));
        } else {
            $res->getBody()->write(json_encode(array("error" => "Your provided authentication token is not valid")));
        }
        return $res;
    }

    if($token->iss !== $_SERVER['SERVER_NAME']) {
        $res->getBody()->write(json_encode(array("error" => "Your provided authentication token was not issued by this service")));
        return $res;
    }

    $now = new DateTimeImmutable();
    $now = $now->getTimestamp();
    if($now < $token->iat || $now > $token->exp) {
        $res->getBody()->write(json_encode(array("error" => "Your provided authentication token is expired")));
        return $res;
    }

    return array('userId' => $token->userId, 'userType' => $token->userType);
}

function genAuthMiddleware($allowedUserTypes) {
    if(is_string($allowedUserTypes))
        $allowedUserTypes = array($allowedUserTypes);
    
    
    return function($req, $handler) use ($allowedUserTypes) {
        $res = verifyJWT($req);
        
        // Something went wrong
        if($res instanceof Psr\Http\Message\ResponseInterface)
            return $res;

        $userType = $res['userType'];


        if(in_array($userType, $allowedUserTypes)) {
            $req = $req->withAttribute('userType', $userType)->withAttribute('userId', $res['userId']);
            return $handler->handle($req);
        }

        throw new HttpNotAuthorizedException();
    };
}

function loginEndpoint($req, $res, $args) {
    $userType = $req->getParsedBody()['userType'];
    $password = $req->getParsedBody()['password'];
    $userData = DB::escapeArray($req->getParsedBody()['userData']);

    if($userType === 'student') {
        $initials = strtoupper($userData['initials']) . '--';
        $password = DB::escape($password);
        $q = DB::queryRaw("SELECT id, prename, name, class FROM students WHERE prename LIKE '{$initials[0]}%' AND name LIKE '{$initials[1]}%' AND class = '{$userData['class']}' AND pwd = '$password'");
        if($q->num_rows > 0) {
            writeLoginAnswerSucc($res, 'student', $q->fetch_assoc());
        } else {
            writeLoginAnswerErr($res);
        }

    } elseif($userType === 'teacher') {
        $q = DB::queryRaw("SELECT * FROM teachers WHERE class = '{$userData['class']}'");
        if($q->num_rows > 0) {
            $dbData = $q->fetch_assoc();

            if(password_verify($password, $dbData['pwd'])) {
                unset($dbData['pwd']);
                writeLoginAnswerSucc($res, 'teacher', $dbData);
            } else {
                writeLoginAnswerErr($res);
            }
        } else {
            writeLoginAnswerErr($res);
        }

    } elseif($userType === 'trainer') {
        $q = DB::queryRaw("SELECT * FROM trainers WHERE name = '{$userData['username']}'");
        if($q->num_rows > 0) {
            $dbData = $q->fetch_assoc();

            if(password_verify($password, $dbData['pwd'])) {
                unset($dbData['pwd']);
                writeLoginAnswerSucc($res, 'trainer', $dbData);
            } else {
                writeLoginAnswerErr($res);
            }
        } else {
            writeLoginAnswerErr($res);
        }

    } elseif($userType === 'other') {
        $q = DB::queryRaw("SELECT * FROM others WHERE name = '{$userData['username']}'");
        if($q->num_rows > 0) {
            $dbData = $q->fetch_assoc();

            if(password_verify($password, $dbData['pwd'])) {
                unset($dbData['pwd']);
                writeLoginAnswerSucc($res, 'other', $dbData);
            } else {
                writeLoginAnswerErr($res);
            }
        } else {
            writeLoginAnswerErr($res);
        }
    } else {
        $res = $res->withStatus(400);
        $res->getBody()->write(json_encode(array("error" => "unknown usertype")));
    }

    return $res;
}

function writeLoginAnswerSucc($res, $userType, $userData) {
    [ $token, $tokenPayload ] = generateJWT($userData['id'], $userType);
    $res->getBody()->write(json_encode(array(
        'auth' => true,
        'token' => $token,
        'userType' => $userType,
        'expire' => $tokenPayload['exp'],
        'userData' => $userData
    )));
}

function writeLoginAnswerErr($res, $msg = 'Passwort oder Anmeldedaten falsch!') {
    $res->getBody()->write(json_encode(array(
        'auth' => false, 
        'message' => $msg
    )));
}
