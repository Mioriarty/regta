<?php
	final class DB {

		private static $mysqli;
		private const BACKUP_DIRECTORY = "../db-backups";

		public static function connect() {
			if(!isset(self::$mysqli)|| self::$mysqli->connect_errno){
				self::$mysqli = new mysqli($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASSWORD'], $_ENV['DB_DATABASE']);
				self::$mysqli->report_mode = MYSQLI_REPORT_ALL;		// Error Report
				self::queryRaw("SET NAMES 'utf8'");

			}
		}

		public static function escapeArray($arr)
		{
			foreach ($arr as $key => $value) {
				$arr[$key] = self::escape($value);
			}
			return $arr;
		}

		public static function escape($string)
		{
			return self::real_escape_string($string);
		}

		public static function real_escape_string($string){
			if(!isset(self::$mysqli)|| self::$mysqli->connect_errno){
				self::connect();
			}
			return self::$mysqli->real_escape_string($string);
		}

		public static function affected_rows() {
			return self::$mysqli->affected_rows;
		}

		public static function disconnect() {
			if(isset(self::$mysqli) || !self::$mysqli->connect_errno){
				self::$mysqli->close();
			}
		}

		public static function getLastInsertId() {
			return self::$mysqli->insert_id;
		}

		public static function query($cmd, $response, $callback) {
			if(!isset(self::$mysqli)|| self::$mysqli->connect_errno){
				self::connect();
			}
			$result = self::$mysqli->query($cmd);
			$body = [];

			if($result) {
				[ $body, $status ] = $callback($result);
			} else {
				$status = 500;
				if(isset($_ENV['DEBUG'])) {
					$body = array("error" => "DB error: " . self::$mysqli->error, "query" => $cmd);
				} else {
					$body = array("error" => "interal error");
				}
			}

			$response = $response->withStatus($status);
			$response->getBody()->write(json_encode($body));
			return $response;
		}

		public static function queryRaw($cmd) {
			if(!isset(self::$mysqli)|| self::$mysqli->connect_errno){
				self::connect();
			}
			return self::$mysqli->query($cmd);
		}

		public static function genSimplePassword($length = 6) {
			$alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Ä', 'Ö', 'Ü',
					     'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ä', 'ö', 'ü',
					     '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', ',', ':', ';', '-', '<', '>', '!', '&', '%', '$', '(', ')', '=', '?', '+', '*', '/', '@'];
			
			$keys = array_rand($alphabet, $length);
			return implode("", array_map(function ($key) use ($alphabet) { return $alphabet[$key]; }, $keys));
		}

		public static function createBackup($description, $automatic) {
			$filename = self::BACKUP_DIRECTORY . '/' . date("YmdHis") . ($automatic ? "-automatic" : "-manuel");

			// create databse dump
			exec($_ENV['MYSQL_DUMP'] . ' -h ' . $_ENV['DB_HOST'] . ' -u ' . $_ENV['DB_USER'] . ' ' . ($_ENV['DB_PASSWORD'] == '' ? '--skip-password ' : "-p'" . $_ENV['DB_PASSWORD'] . "' ") . $_ENV['DB_DATABASE'] . ' > ' . $filename . '.sql', $res);
			
			
			if($description != "") {
				// create description file
				$descFile = fopen($filename . "_description.txt", "w");
				fwrite($descFile, $description);
				fclose($descFile);
			}
			return $filename;
		}

		public static function getStoredBackups() {
			$backups = [];

			foreach(new DirectoryIterator(DB::BACKUP_DIRECTORY) as $file) {
				if($file->isDot() || $file->getExtension() != "sql") continue;
				$description = "";

				// check whether decription file exists
				$descriptionPath = substr($file->getPathname(), 0, -4) . "_description.txt";
				if(file_exists($descriptionPath)) {
					$description = file_get_contents($descriptionPath);
				}
				
				$backups[] = array(
					"fileInfo" => clone $file, 
					"description" => $description, 
					"automatic" => substr($file->getBasename(".sql"), -9) == "automatic"
				);
			}
			
			return $backups;
		}

		public static function deleteBackup($filename) {
			$filename = substr($filename, -4) == ".sql" ? substr($filename, 0 -4) : $filename;

			unlink(self::BACKUP_DIRECTORY . '/' . $filename . '.sql');
			unlink(self::BACKUP_DIRECTORY . '/' . $filename . '_description.txt');
		}

		public static function getBackupPath($filename) {
			$filename = substr($filename, -4) == ".sql" ? substr($filename, 0 -4) : $filename;
			return self::BACKUP_DIRECTORY . '/' . $filename . '.sql';
		}

		// ******* CALLBACKS *************

		public static function CB_GET($q) {
			$res = [];
			while($row = $q->fetch_assoc()) {
				$res[] = $row;
			}
			return [ $res, 200 ];
		}

		public static function CB_POST($q) {
			if(self::affected_rows() > 0){
				return [ array("insertId" => self::getLastInsertId(), "status" => "ok"), 201 ];
			} else {
				return [ array("error" => "internal error", "status" => "failed"), 500 ];
			}
		}

		public static function CB_PUT($q) {
			if(self::affected_rows() > 0) {
				return [ array("status" => "ok"), 200 ];
			} else {
				return [ array("message" => "no entity was modified", "status" => "ok"), 200 ];
			}
		}

		public static function CB_DELETE($q) {
			if(self::affected_rows() > 0) {
				return [ array("status" => "ok"), 200 ];
			} else {
				return [ array("message" => "no entity was deleted", "status" => "ok"), 200 ];
			}
		}


		public static function CB_ALWAYS_OK($q) {
			return [ array("status" => "ok"), 200 ];
		}
	}

?>
